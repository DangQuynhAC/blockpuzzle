// <auto-generated>
// THIS (.cs) FILE IS GENERATED BY MPC(MessagePack-CSharp). DO NOT CHANGE IT.
// </auto-generated>

#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168

#pragma warning disable SA1200 // Using directives should be placed correctly
#pragma warning disable SA1312 // Variable names should begin with lower-case letter
#pragma warning disable SA1649 // File name should match first type name

namespace MessagePack.Resolvers
{
    using System;

    public class GeneratedResolver : global::MessagePack.IFormatterResolver
    {
        public static readonly global::MessagePack.IFormatterResolver Instance = new GeneratedResolver();

        private GeneratedResolver()
        {
        }

        public global::MessagePack.Formatters.IMessagePackFormatter<T> GetFormatter<T>()
        {
            return FormatterCache<T>.Formatter;
        }

        private static class FormatterCache<T>
        {
            internal static readonly global::MessagePack.Formatters.IMessagePackFormatter<T> Formatter;

            static FormatterCache()
            {
                var f = GeneratedResolverGetFormatterHelper.GetFormatter(typeof(T));
                if (f != null)
                {
                    Formatter = (global::MessagePack.Formatters.IMessagePackFormatter<T>)f;
                }
            }
        }
    }

    internal static class GeneratedResolverGetFormatterHelper
    {
        private static readonly global::System.Collections.Generic.Dictionary<Type, int> lookup;

        static GeneratedResolverGetFormatterHelper()
        {
            lookup = new global::System.Collections.Generic.Dictionary<Type, int>(12)
            {
                { typeof(global::BPLogic.BPBlock[]), 0 },
                { typeof(global::BPLogic.BlockType), 1 },
                { typeof(global::BPData.ProfileData.BoosterData), 2 },
                { typeof(global::BPData.ProfileData.CollectibleData), 3 },
                { typeof(global::BPData.ProfileData.ProfileData), 4 },
                { typeof(global::BPData.ProfileData.ScoreData), 5 },
                { typeof(global::BPData.ProfileData.TrophyData), 6 },
                { typeof(global::BPLogic.BPBlock), 7 },
                { typeof(global::BPLogic.BPBlockGenerator), 8 },
                { typeof(global::BPLogic.BPBlockLevelGenerator), 9 },
                { typeof(global::BPLogic.BPBoard), 10 },
                { typeof(global::BPLogic.BPGame), 11 },
            };
        }

        internal static object GetFormatter(Type t)
        {
            int key;
            if (!lookup.TryGetValue(t, out key))
            {
                return null;
            }

            switch (key)
            {
                case 0: return new global::MessagePack.Formatters.ArrayFormatter<global::BPLogic.BPBlock>();
                case 1: return new MessagePack.Formatters.BPLogic.BlockTypeFormatter();
                case 2: return new MessagePack.Formatters.BPData.ProfileData.BoosterDataFormatter();
                case 3: return new MessagePack.Formatters.BPData.ProfileData.CollectibleDataFormatter();
                case 4: return new MessagePack.Formatters.BPData.ProfileData.ProfileDataFormatter();
                case 5: return new MessagePack.Formatters.BPData.ProfileData.ScoreDataFormatter();
                case 6: return new MessagePack.Formatters.BPData.ProfileData.TrophyDataFormatter();
                case 7: return new MessagePack.Formatters.BPLogic.BPBlockFormatter();
                case 8: return new MessagePack.Formatters.BPLogic.BPBlockGeneratorFormatter();
                case 9: return new MessagePack.Formatters.BPLogic.BPBlockLevelGeneratorFormatter();
                case 10: return new MessagePack.Formatters.BPLogic.BPBoardFormatter();
                case 11: return new MessagePack.Formatters.BPLogic.BPGameFormatter();
                default: return null;
            }
        }
    }
}

#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612

#pragma warning restore SA1312 // Variable names should begin with lower-case letter
#pragma warning restore SA1200 // Using directives should be placed correctly
#pragma warning restore SA1649 // File name should match first type name


// <auto-generated>
// THIS (.cs) FILE IS GENERATED BY MPC(MessagePack-CSharp). DO NOT CHANGE IT.
// </auto-generated>

#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168

#pragma warning disable SA1200 // Using directives should be placed correctly
#pragma warning disable SA1403 // File may only contain a single namespace
#pragma warning disable SA1649 // File name should match first type name

namespace MessagePack.Formatters.BPLogic
{
    using System;
    using System.Buffers;
    using MessagePack;

    public sealed class BlockTypeFormatter : global::MessagePack.Formatters.IMessagePackFormatter<global::BPLogic.BlockType>
    {
        public void Serialize(ref MessagePackWriter writer, global::BPLogic.BlockType value, global::MessagePack.MessagePackSerializerOptions options)
        {
            writer.Write((Int32)value);
        }

        public global::BPLogic.BlockType Deserialize(ref MessagePackReader reader, global::MessagePack.MessagePackSerializerOptions options)
        {
            return (global::BPLogic.BlockType)reader.ReadInt32();
        }
    }
}

#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612

#pragma warning restore SA1200 // Using directives should be placed correctly
#pragma warning restore SA1403 // File may only contain a single namespace
#pragma warning restore SA1649 // File name should match first type name



// <auto-generated>
// THIS (.cs) FILE IS GENERATED BY MPC(MessagePack-CSharp). DO NOT CHANGE IT.
// </auto-generated>

#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168

#pragma warning disable SA1129 // Do not use default value type constructor
#pragma warning disable SA1200 // Using directives should be placed correctly
#pragma warning disable SA1309 // Field names should not begin with underscore
#pragma warning disable SA1312 // Variable names should begin with lower-case letter
#pragma warning disable SA1403 // File may only contain a single namespace
#pragma warning disable SA1649 // File name should match first type name

namespace MessagePack.Formatters.BPData.ProfileData
{
    using System;
    using System.Buffers;
    using MessagePack;

    public sealed class BoosterDataFormatter : global::MessagePack.Formatters.IMessagePackFormatter<global::BPData.ProfileData.BoosterData>
    {

        public void Serialize(ref MessagePackWriter writer, global::BPData.ProfileData.BoosterData value, global::MessagePack.MessagePackSerializerOptions options)
        {
            writer.WriteArrayHeader(1);
            writer.Write(value.bombCount);
        }

        public global::BPData.ProfileData.BoosterData Deserialize(ref MessagePackReader reader, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (reader.TryReadNil())
            {
                throw new InvalidOperationException("typecode is null, struct not supported");
            }

            options.Security.DepthStep(ref reader);
            var length = reader.ReadArrayHeader();
            var __bombCount__ = default(int);

            for (int i = 0; i < length; i++)
            {
                switch (i)
                {
                    case 0:
                        __bombCount__ = reader.ReadInt32();
                        break;
                    default:
                        reader.Skip();
                        break;
                }
            }

            var ____result = new global::BPData.ProfileData.BoosterData(__bombCount__);
            ____result.bombCount = __bombCount__;
            reader.Depth--;
            return ____result;
        }
    }

    public sealed class CollectibleDataFormatter : global::MessagePack.Formatters.IMessagePackFormatter<global::BPData.ProfileData.CollectibleData>
    {

        public void Serialize(ref MessagePackWriter writer, global::BPData.ProfileData.CollectibleData value, global::MessagePack.MessagePackSerializerOptions options)
        {
            writer.WriteArrayHeader(3);
            writer.Write(value.collectibleTarget);
            writer.Write(value.collectibleCurrent);
            writer.Write(value.collectibleTotal);
        }

        public global::BPData.ProfileData.CollectibleData Deserialize(ref MessagePackReader reader, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (reader.TryReadNil())
            {
                throw new InvalidOperationException("typecode is null, struct not supported");
            }

            options.Security.DepthStep(ref reader);
            var length = reader.ReadArrayHeader();
            var __collectibleTarget__ = default(int);
            var __collectibleCurrent__ = default(int);
            var __collectibleTotal__ = default(int);

            for (int i = 0; i < length; i++)
            {
                switch (i)
                {
                    case 0:
                        __collectibleTarget__ = reader.ReadInt32();
                        break;
                    case 1:
                        __collectibleCurrent__ = reader.ReadInt32();
                        break;
                    case 2:
                        __collectibleTotal__ = reader.ReadInt32();
                        break;
                    default:
                        reader.Skip();
                        break;
                }
            }

            var ____result = new global::BPData.ProfileData.CollectibleData(__collectibleTarget__, __collectibleCurrent__, __collectibleTotal__);
            ____result.collectibleTarget = __collectibleTarget__;
            ____result.collectibleCurrent = __collectibleCurrent__;
            ____result.collectibleTotal = __collectibleTotal__;
            reader.Depth--;
            return ____result;
        }
    }

    public sealed class ProfileDataFormatter : global::MessagePack.Formatters.IMessagePackFormatter<global::BPData.ProfileData.ProfileData>
    {

        public void Serialize(ref MessagePackWriter writer, global::BPData.ProfileData.ProfileData value, global::MessagePack.MessagePackSerializerOptions options)
        {
            IFormatterResolver formatterResolver = options.Resolver;
            writer.WriteArrayHeader(5);
            formatterResolver.GetFormatterWithVerify<global::BPLogic.BPGame>().Serialize(ref writer, value.game, options);
            formatterResolver.GetFormatterWithVerify<global::BPData.ProfileData.ScoreData>().Serialize(ref writer, value.scoreData, options);
            formatterResolver.GetFormatterWithVerify<global::BPData.ProfileData.CollectibleData>().Serialize(ref writer, value.collectibleData, options);
            formatterResolver.GetFormatterWithVerify<global::BPData.ProfileData.BoosterData>().Serialize(ref writer, value.boosterData, options);
            formatterResolver.GetFormatterWithVerify<global::BPData.ProfileData.TrophyData>().Serialize(ref writer, value.trophyData, options);
        }

        public global::BPData.ProfileData.ProfileData Deserialize(ref MessagePackReader reader, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (reader.TryReadNil())
            {
                throw new InvalidOperationException("typecode is null, struct not supported");
            }

            options.Security.DepthStep(ref reader);
            IFormatterResolver formatterResolver = options.Resolver;
            var length = reader.ReadArrayHeader();
            var __game__ = default(global::BPLogic.BPGame);
            var __scoreData__ = default(global::BPData.ProfileData.ScoreData);
            var __collectibleData__ = default(global::BPData.ProfileData.CollectibleData);
            var __boosterData__ = default(global::BPData.ProfileData.BoosterData);
            var __trophyData__ = default(global::BPData.ProfileData.TrophyData);

            for (int i = 0; i < length; i++)
            {
                switch (i)
                {
                    case 0:
                        __game__ = formatterResolver.GetFormatterWithVerify<global::BPLogic.BPGame>().Deserialize(ref reader, options);
                        break;
                    case 1:
                        __scoreData__ = formatterResolver.GetFormatterWithVerify<global::BPData.ProfileData.ScoreData>().Deserialize(ref reader, options);
                        break;
                    case 2:
                        __collectibleData__ = formatterResolver.GetFormatterWithVerify<global::BPData.ProfileData.CollectibleData>().Deserialize(ref reader, options);
                        break;
                    case 3:
                        __boosterData__ = formatterResolver.GetFormatterWithVerify<global::BPData.ProfileData.BoosterData>().Deserialize(ref reader, options);
                        break;
                    case 4:
                        __trophyData__ = formatterResolver.GetFormatterWithVerify<global::BPData.ProfileData.TrophyData>().Deserialize(ref reader, options);
                        break;
                    default:
                        reader.Skip();
                        break;
                }
            }

            var ____result = new global::BPData.ProfileData.ProfileData(__game__, __scoreData__, __collectibleData__, __boosterData__, __trophyData__);
            reader.Depth--;
            return ____result;
        }
    }

    public sealed class ScoreDataFormatter : global::MessagePack.Formatters.IMessagePackFormatter<global::BPData.ProfileData.ScoreData>
    {

        public void Serialize(ref MessagePackWriter writer, global::BPData.ProfileData.ScoreData value, global::MessagePack.MessagePackSerializerOptions options)
        {
            writer.WriteArrayHeader(2);
            writer.Write(value.currentScore);
            writer.Write(value.bestScore);
        }

        public global::BPData.ProfileData.ScoreData Deserialize(ref MessagePackReader reader, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (reader.TryReadNil())
            {
                throw new InvalidOperationException("typecode is null, struct not supported");
            }

            options.Security.DepthStep(ref reader);
            var length = reader.ReadArrayHeader();
            var __currentScore__ = default(int);
            var __bestScore__ = default(int);

            for (int i = 0; i < length; i++)
            {
                switch (i)
                {
                    case 0:
                        __currentScore__ = reader.ReadInt32();
                        break;
                    case 1:
                        __bestScore__ = reader.ReadInt32();
                        break;
                    default:
                        reader.Skip();
                        break;
                }
            }

            var ____result = new global::BPData.ProfileData.ScoreData(__currentScore__, __bestScore__);
            ____result.currentScore = __currentScore__;
            ____result.bestScore = __bestScore__;
            reader.Depth--;
            return ____result;
        }
    }

    public sealed class TrophyDataFormatter : global::MessagePack.Formatters.IMessagePackFormatter<global::BPData.ProfileData.TrophyData>
    {

        public void Serialize(ref MessagePackWriter writer, global::BPData.ProfileData.TrophyData value, global::MessagePack.MessagePackSerializerOptions options)
        {
            writer.WriteArrayHeader(3);
            writer.Write(value.targetTrophies);
            writer.Write(value.currentTrophies);
            writer.Write(value.totalTrophies);
        }

        public global::BPData.ProfileData.TrophyData Deserialize(ref MessagePackReader reader, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (reader.TryReadNil())
            {
                throw new InvalidOperationException("typecode is null, struct not supported");
            }

            options.Security.DepthStep(ref reader);
            var length = reader.ReadArrayHeader();
            var __targetTrophies__ = default(int);
            var __currentTrophies__ = default(int);
            var __totalTrophies__ = default(int);

            for (int i = 0; i < length; i++)
            {
                switch (i)
                {
                    case 0:
                        __targetTrophies__ = reader.ReadInt32();
                        break;
                    case 1:
                        __currentTrophies__ = reader.ReadInt32();
                        break;
                    case 2:
                        __totalTrophies__ = reader.ReadInt32();
                        break;
                    default:
                        reader.Skip();
                        break;
                }
            }

            var ____result = new global::BPData.ProfileData.TrophyData(__targetTrophies__, __currentTrophies__, __totalTrophies__);
            ____result.targetTrophies = __targetTrophies__;
            ____result.currentTrophies = __currentTrophies__;
            ____result.totalTrophies = __totalTrophies__;
            reader.Depth--;
            return ____result;
        }
    }
}

#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612

#pragma warning restore SA1129 // Do not use default value type constructor
#pragma warning restore SA1200 // Using directives should be placed correctly
#pragma warning restore SA1309 // Field names should not begin with underscore
#pragma warning restore SA1312 // Variable names should begin with lower-case letter
#pragma warning restore SA1403 // File may only contain a single namespace
#pragma warning restore SA1649 // File name should match first type name

// <auto-generated>
// THIS (.cs) FILE IS GENERATED BY MPC(MessagePack-CSharp). DO NOT CHANGE IT.
// </auto-generated>

#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168

#pragma warning disable SA1129 // Do not use default value type constructor
#pragma warning disable SA1200 // Using directives should be placed correctly
#pragma warning disable SA1309 // Field names should not begin with underscore
#pragma warning disable SA1312 // Variable names should begin with lower-case letter
#pragma warning disable SA1403 // File may only contain a single namespace
#pragma warning disable SA1649 // File name should match first type name

namespace MessagePack.Formatters.BPLogic
{
    using System;
    using System.Buffers;
    using MessagePack;

    public sealed class BPBlockFormatter : global::MessagePack.Formatters.IMessagePackFormatter<global::BPLogic.BPBlock>
    {

        public void Serialize(ref MessagePackWriter writer, global::BPLogic.BPBlock value, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (value == null)
            {
                writer.WriteNil();
                return;
            }

            IFormatterResolver formatterResolver = options.Resolver;
            writer.WriteArrayHeader(4);
            writer.Write(value.TilesCount);
            formatterResolver.GetFormatterWithVerify<int[]>().Serialize(ref writer, value.data, options);
            formatterResolver.GetFormatterWithVerify<global::BPLogic.BlockType>().Serialize(ref writer, value.type, options);
            writer.Write(value.isAvailable);
        }

        public global::BPLogic.BPBlock Deserialize(ref MessagePackReader reader, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (reader.TryReadNil())
            {
                return null;
            }

            options.Security.DepthStep(ref reader);
            IFormatterResolver formatterResolver = options.Resolver;
            var length = reader.ReadArrayHeader();
            var __TilesCount__ = default(int);
            var __data__ = default(int[]);
            var __type__ = default(global::BPLogic.BlockType);
            var __isAvailable__ = default(bool);

            for (int i = 0; i < length; i++)
            {
                switch (i)
                {
                    case 0:
                        __TilesCount__ = reader.ReadInt32();
                        break;
                    case 1:
                        __data__ = formatterResolver.GetFormatterWithVerify<int[]>().Deserialize(ref reader, options);
                        break;
                    case 2:
                        __type__ = formatterResolver.GetFormatterWithVerify<global::BPLogic.BlockType>().Deserialize(ref reader, options);
                        break;
                    case 3:
                        __isAvailable__ = reader.ReadBoolean();
                        break;
                    default:
                        reader.Skip();
                        break;
                }
            }

            var ____result = new global::BPLogic.BPBlock(__TilesCount__, __data__, __type__, __isAvailable__);
            ____result.type = __type__;
            ____result.isAvailable = __isAvailable__;
            reader.Depth--;
            return ____result;
        }
    }

    public sealed class BPBlockGeneratorFormatter : global::MessagePack.Formatters.IMessagePackFormatter<global::BPLogic.BPBlockGenerator>
    {

        public void Serialize(ref MessagePackWriter writer, global::BPLogic.BPBlockGenerator value, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (value == null)
            {
                writer.WriteNil();
                return;
            }

            writer.WriteArrayHeader(0);
        }

        public global::BPLogic.BPBlockGenerator Deserialize(ref MessagePackReader reader, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (reader.TryReadNil())
            {
                return null;
            }

            options.Security.DepthStep(ref reader);
            var length = reader.ReadArrayHeader();

            for (int i = 0; i < length; i++)
            {
                switch (i)
                {
                    default:
                        reader.Skip();
                        break;
                }
            }

            var ____result = new global::BPLogic.BPBlockGenerator();
            reader.Depth--;
            return ____result;
        }
    }

    public sealed class BPBlockLevelGeneratorFormatter : global::MessagePack.Formatters.IMessagePackFormatter<global::BPLogic.BPBlockLevelGenerator>
    {

        public void Serialize(ref MessagePackWriter writer, global::BPLogic.BPBlockLevelGenerator value, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (value == null)
            {
                writer.WriteNil();
                return;
            }

            writer.WriteArrayHeader(0);
        }

        public global::BPLogic.BPBlockLevelGenerator Deserialize(ref MessagePackReader reader, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (reader.TryReadNil())
            {
                return null;
            }

            options.Security.DepthStep(ref reader);
            var length = reader.ReadArrayHeader();

            for (int i = 0; i < length; i++)
            {
                switch (i)
                {
                    default:
                        reader.Skip();
                        break;
                }
            }

            var ____result = new global::BPLogic.BPBlockLevelGenerator();
            reader.Depth--;
            return ____result;
        }
    }

    public sealed class BPBoardFormatter : global::MessagePack.Formatters.IMessagePackFormatter<global::BPLogic.BPBoard>
    {

        public void Serialize(ref MessagePackWriter writer, global::BPLogic.BPBoard value, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (value == null)
            {
                writer.WriteNil();
                return;
            }

            IFormatterResolver formatterResolver = options.Resolver;
            writer.WriteArrayHeader(2);
            formatterResolver.GetFormatterWithVerify<int[]>().Serialize(ref writer, value.data, options);
            formatterResolver.GetFormatterWithVerify<int[]>().Serialize(ref writer, value.maskData, options);
        }

        public global::BPLogic.BPBoard Deserialize(ref MessagePackReader reader, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (reader.TryReadNil())
            {
                return null;
            }

            options.Security.DepthStep(ref reader);
            IFormatterResolver formatterResolver = options.Resolver;
            var length = reader.ReadArrayHeader();
            var __data__ = default(int[]);
            var __maskData__ = default(int[]);

            for (int i = 0; i < length; i++)
            {
                switch (i)
                {
                    case 0:
                        __data__ = formatterResolver.GetFormatterWithVerify<int[]>().Deserialize(ref reader, options);
                        break;
                    case 1:
                        __maskData__ = formatterResolver.GetFormatterWithVerify<int[]>().Deserialize(ref reader, options);
                        break;
                    default:
                        reader.Skip();
                        break;
                }
            }

            var ____result = new global::BPLogic.BPBoard(__data__, __maskData__);
            ____result.data = __data__;
            ____result.maskData = __maskData__;
            reader.Depth--;
            return ____result;
        }
    }

    public sealed class BPGameFormatter : global::MessagePack.Formatters.IMessagePackFormatter<global::BPLogic.BPGame>
    {

        public void Serialize(ref MessagePackWriter writer, global::BPLogic.BPGame value, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (value == null)
            {
                writer.WriteNil();
                return;
            }

            IFormatterResolver formatterResolver = options.Resolver;
            writer.WriteArrayHeader(4);
            formatterResolver.GetFormatterWithVerify<global::BPLogic.BPBlock[]>().Serialize(ref writer, value.blocks, options);
            formatterResolver.GetFormatterWithVerify<global::BPLogic.BPBoard>().Serialize(ref writer, value.board, options);
            formatterResolver.GetFormatterWithVerify<global::BPLogic.BPBlockGenerator>().Serialize(ref writer, value.blockGenerator, options);
            formatterResolver.GetFormatterWithVerify<global::BPLogic.BPBlockLevelGenerator>().Serialize(ref writer, value.blockLevelGenerator, options);
        }

        public global::BPLogic.BPGame Deserialize(ref MessagePackReader reader, global::MessagePack.MessagePackSerializerOptions options)
        {
            if (reader.TryReadNil())
            {
                return null;
            }

            options.Security.DepthStep(ref reader);
            IFormatterResolver formatterResolver = options.Resolver;
            var length = reader.ReadArrayHeader();
            var __blocks__ = default(global::BPLogic.BPBlock[]);
            var __board__ = default(global::BPLogic.BPBoard);
            var __blockGenerator__ = default(global::BPLogic.BPBlockGenerator);
            var __blockLevelGenerator__ = default(global::BPLogic.BPBlockLevelGenerator);

            for (int i = 0; i < length; i++)
            {
                switch (i)
                {
                    case 0:
                        __blocks__ = formatterResolver.GetFormatterWithVerify<global::BPLogic.BPBlock[]>().Deserialize(ref reader, options);
                        break;
                    case 1:
                        __board__ = formatterResolver.GetFormatterWithVerify<global::BPLogic.BPBoard>().Deserialize(ref reader, options);
                        break;
                    case 2:
                        __blockGenerator__ = formatterResolver.GetFormatterWithVerify<global::BPLogic.BPBlockGenerator>().Deserialize(ref reader, options);
                        break;
                    case 3:
                        __blockLevelGenerator__ = formatterResolver.GetFormatterWithVerify<global::BPLogic.BPBlockLevelGenerator>().Deserialize(ref reader, options);
                        break;
                    default:
                        reader.Skip();
                        break;
                }
            }

            var ____result = new global::BPLogic.BPGame(__blocks__, __board__, __blockGenerator__, __blockLevelGenerator__);
            ____result.blocks = __blocks__;
            ____result.board = __board__;
            ____result.blockGenerator = __blockGenerator__;
            ____result.blockLevelGenerator = __blockLevelGenerator__;
            reader.Depth--;
            return ____result;
        }
    }
}

#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612

#pragma warning restore SA1129 // Do not use default value type constructor
#pragma warning restore SA1200 // Using directives should be placed correctly
#pragma warning restore SA1309 // Field names should not begin with underscore
#pragma warning restore SA1312 // Variable names should begin with lower-case letter
#pragma warning restore SA1403 // File may only contain a single namespace
#pragma warning restore SA1649 // File name should match first type name

