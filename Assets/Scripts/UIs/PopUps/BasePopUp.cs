﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BPUI.PopUps
{
    [RequireComponent(typeof(PopUpMotion))]
    public abstract class BasePopUp : MonoBehaviour
    {
        public bool shouldShowBlackImage;

        public bool allowMultiplePopUps;

        public abstract EPopUpID PopUpID { get; }

        public event Action onBeginShowing = delegate { };

        public event Action onBeginHiding = delegate { };

        public event Action onShown = delegate { };

        public event Action onHidden = delegate { };

        private PopUpMotion popUpMotion;

        public virtual void Show(params object[] data)
        {
            if (popUpMotion == null)
            {
                popUpMotion = GetComponent<PopUpMotion>();
            }

            popUpMotion.onCompleteShowing += OnShown;
            popUpMotion.onCompleteHiding += OnHidden;

            onBeginShowing();
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            onBeginHiding();
            gameObject.SetActive(false);
        }

        protected void OnShown()
        {
            onShown();
        }

        protected void OnHidden()
        {
            onHidden();
            
            popUpMotion.onCompleteShowing -= OnShown;
            popUpMotion.onCompleteHiding -= OnHidden;
        }
    }
}