﻿using System;
using System.Collections;
using System.Collections.Generic;
using BPCore;
using UnityEngine;
using UnityEngine.UI;

namespace BPUI.PopUps
{
    public class PopUpBombShop : BasePopUp
    {
        public override EPopUpID PopUpID => EPopUpID.BombShop;

        private List<BombItemInfo> infos;
        private List<BombShopElement> bombShopElements;

        private IBoosterBombCallbackHandler bombCallbackHandler;
        private ICollectibleCallbackHandler collectibleCallbackHandler;

        private PopUpController popUpController;

        [SerializeField] private Transform contentRoot;
        [SerializeField] private BombShopElement sample;

        [SerializeField] private Button buttonClose;

        public override void Show(params object[] data)
        {
            if (data != null)
            {
                bombCallbackHandler = data[0] as IBoosterBombCallbackHandler;
                collectibleCallbackHandler = data[1] as ICollectibleCallbackHandler;
            }

            InitShopData();

            bombShopElements = new List<BombShopElement>();

            InitView();

            popUpController = FindObjectOfType<PopUpController>();
            buttonClose.onClick.AddListener(() => popUpController.Hide(PopUpID));
            
            base.Show(data);
        }

        public override void Hide()
        {
            buttonClose.onClick.RemoveAllListeners();

            infos = null;

            for (int i = 0; i < bombShopElements.Count; i++)
            {
                Destroy(bombShopElements[i].gameObject);
            }

            bombShopElements = null;

            base.Hide();
        }

        private void InitShopData()
        {
            infos = new List<BombItemInfo>
            {
                new BombItemInfo(1, 900),
                new BombItemInfo(3, 1900),
                new BombItemInfo(9, 4900),
                new BombItemInfo(18, 8900)
            };
        }

        private void InitView()
        {
            for (int i = 0; i < infos.Count; i++)
            {
                SetupAnElement(infos[i], i);
            }
        }

        private void SetupAnElement(BombItemInfo info, int id)
        {
            var element = Instantiate(sample, contentRoot, true);
            element.Init(info, id, HandlePurchase);
            element.gameObject.SetActive(true);
            element.transform.localScale = Vector3.one;
            bombShopElements.Add(element);
        }

        private void HandlePurchase(int itemId)
        {
            var info = infos[itemId];
            if (collectibleCallbackHandler.IsEnoughCollectibleForMakingPurchase(info.collectiblesCost))
            {
                collectibleCallbackHandler.SpendCollectibles(info.collectiblesCost);
                bombCallbackHandler.AddBombs(info.bomb);
            }
        }
    }

    public struct BombItemInfo
    {
        public int bomb;
        public int collectiblesCost;

        public BombItemInfo(int bomb, int collectiblesCost)
        {
            this.bomb = bomb;
            this.collectiblesCost = collectiblesCost;
        }
    }
}