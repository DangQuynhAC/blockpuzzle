﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace BPUI.PopUps
{
    public class PopUpMotion : MonoBehaviour
    {
        public event Action onCompleteShowing = delegate { };
        public event Action onCompleteHiding = delegate { };

        private void OnEnable()
        {
            transform.DOScale(1f, 0.5f).OnComplete(() => onCompleteShowing());
        }

        private void OnDisable()
        {
            transform.DOScale(0f, 0.5f).OnComplete(() => onCompleteHiding());
        }
    }
}