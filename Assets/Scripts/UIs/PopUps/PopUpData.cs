﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BPUI.PopUps
{
    public struct PopUpData
    {
        public readonly EPopUpID popUpID;
        public readonly object[] data;

        public PopUpData(EPopUpID popUpID, params object[] data)
        {
            this.popUpID = popUpID;
            this.data = data;
        }
    }
}