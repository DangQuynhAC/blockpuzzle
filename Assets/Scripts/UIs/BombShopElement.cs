﻿using System;
using System.Collections;
using System.Collections.Generic;
using BPUI.PopUps;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BPUI
{
    public class BombShopElement : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI bombCount;

        [SerializeField] private TextMeshProUGUI collectiblesRequired;

        [SerializeField] private Button buttonBuy;


        public void Init(BombItemInfo info, int id, Action<int> onPurchaseButtonClicked)
        {
            bombCount.text = info.bomb.ToString();
            collectiblesRequired.text = info.collectiblesCost.ToString();
            buttonBuy.onClick.AddListener(() => onPurchaseButtonClicked(id));
        }
    }
}