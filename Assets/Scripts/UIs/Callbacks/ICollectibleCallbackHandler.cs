﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BPUI
{
    public interface ICollectibleCallbackHandler
    {
        void SpendCollectibles(int amount);
        bool IsEnoughCollectibleForMakingPurchase(int collectibleRequired);
    }

}