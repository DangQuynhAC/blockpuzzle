﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BPUI
{
    public interface IBoosterBombCallbackHandler
    {
        void AddBombs(int amount);
    }

}