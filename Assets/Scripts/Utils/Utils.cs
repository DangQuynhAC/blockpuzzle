﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public static class Utils
    {
        private static System.Random rng = new System.Random();  

        public static void Shuffle<T>(this IList<T> list)  
        {  
            int n = list.Count;  
            while (n > 1) {  
                n--;  
                int k = rng.Next(n + 1);  
                T value = list[k];  
                list[k] = list[n];  
                list[n] = value;  
            }  
        }
    }

    public static class Extensions
    {
        public static Vector2Int OffsetY(this Vector2Int v, int i)
        {
            Vector2Int res = v;
            res.y += i;
            return res;
        }

        public static Vector2Int OffsetX(this Vector2Int v, int i)
        {
            Vector2Int res = v;
            res.x += i;
            return res;
        }

        public static Vector2Int Left(this  Vector2Int v)
        {
            Vector2Int res = v;
            res.x -= 1;
            return res;
        }
        
        public static Vector2Int Right(this  Vector2Int v)
        {
            Vector2Int res = v;
            res.x += 1;
            return res;
        }
        
        public static Vector2Int Up(this  Vector2Int v)
        {
            Vector2Int res = v;
            res.y -= 1;
            return res;
        }
        
        public static Vector2Int Down(this  Vector2Int v)
        {
            Vector2Int res = v;
            res.y += 1;
            return res;
        }
    }
}