﻿using System.Collections;
using System.Collections.Generic;
using BPData.ProfileData;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Base
{
    [RequireComponent(typeof(BoxCollider2D))]
    public abstract class BaseDraggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler
    {
        
        protected Camera mainCamera;

        protected BoxCollider2D hitbox;

        public abstract void Init();
        public abstract void OnBeginDrag(PointerEventData eventData);

        public abstract void OnDrag(PointerEventData eventData);

        public abstract void OnEndDrag(PointerEventData eventData);

        public abstract void OnPointerClick(PointerEventData eventData);
    }

}