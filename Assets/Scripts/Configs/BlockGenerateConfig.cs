﻿using System;
using System.Collections;
using System.Collections.Generic;
using BPLogic;
using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "BlockConfig", menuName =  "Config/BlockConfig")]
    public class BlockGenerateConfig : ScriptableObject
    {
        [SerializeField]
        private List<ScoreBasedConfig> configs;

        public List<ScoreBasedConfig> Configs => configs;
    }

    [Serializable]
    public class ScoreBasedConfig
    {
        public int targetScore;
        public List<BPBlockLevelConfig> levelConfig;
        public BlockConfig blockConfig;
    }
    
    [Serializable]
    public class BlockConfig
    {
        public int block1x1Rate;
        public int block1x2Rate;
        public int block1x3Rate;
        public int block1x4Rate;
        public int block2x2LRate;
        public int block2x3LRate;
        public int block3x2LRate;
        public int block2x3TRate;
        public int block2x2Rate;
        public int block2x3ZRate;
        public int block2x3ZRRate;
    }
}