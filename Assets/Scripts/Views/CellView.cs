﻿using BPCore;
using BPPools;
using UnityEngine;

namespace BPViews
{
    public class CellView : MonoBehaviour, IPoolable
    {
        private SpriteRenderer spriteRenderer;
        private TileView tileView;

        public bool CellReady => tileView != null;


        public void SetView(TileView view)
        {
            tileView = view;
            tileView.SetPosition(transform.position);
        }

        public void UpdateView(int newValue)
        {
            tileView.UpdateTileLevel(newValue);
        }

        public void RemoveView()
        {
            if (tileView != null)
            {
                PoolController.Destroy(tileView);
                tileView = null;
            }
        }

        public void Highlight()
        {
            spriteRenderer.color = Color.cyan;
        }

        public void DeEmphasize()
        {
            spriteRenderer.color = Color.white;
        }

        public void SetPosition(Vector3 position)
        {
            transform.position = position;
        }

        public void Active()
        {
            if (spriteRenderer == null)
            {
                spriteRenderer = GetComponent<SpriteRenderer>();
            }

            gameObject.SetActive(true);
        }

        public void Dispose()
        {
            tileView = null;
            gameObject.SetActive(false);
        }
    }
}