﻿using System;
using System.Collections;
using System.Collections.Generic;
using BPCore;
using BPLogic;
using UnityEngine;

namespace BPViews
{
    public class BlockView : MonoBehaviour
    {
        private List<TileView> tileViews;

        public void Init()
        {
            tileViews = new List<TileView>();
        }

        public void ResetView()
        {
            if (tileViews != null)
            {
                for (int i = 0; i < tileViews.Count; i++)
                {
                    PoolController.Destroy(tileViews[i]);
                }
            }
            
            ClearView();
        }

        public void ClearView()
        {
            tileViews.Clear();
        }

        public void UpdateView(int[] viewData)
        {
            SetUpView(viewData);
        }

        public List<TileView> GetViews()
        {
            return tileViews;
        }

        public void RotateView()
        {
            for (int i = 0; i < tileViews.Count; i++)
            {
                var currentView = tileViews[i].transform.position;

                var rotatedX = currentView.x * Mathf.Cos(-Mathf.PI / 2) - currentView.y * Mathf.Sin(-Mathf.PI / 2);
                rotatedX = Mathf.Round(rotatedX * 1000f) * 0.001f;

                var rotatedY = currentView.x * Mathf.Sin(-Mathf.PI / 2) + currentView.y * Mathf.Cos(-Mathf.PI / 2);
                rotatedY = Mathf.Round(rotatedY * 1000f) * 0.001f;

                tileViews[i].SetPosition(new Vector3(rotatedX, rotatedY));
            }

            NormalizeView();
        }

        public void SetMovingPosition(Vector3 worldPointer)
        {
            worldPointer.y += GameViewConfigs.MOVING_OFFSET;
            worldPointer.z = -10;

            for (int i = 0; i < tileViews.Count; i++)
            {
                tileViews[i].ApplyScale(GameViewConfigs.BLOCK_DRAGGING_SCALE);
                tileViews[i].SetDelta(worldPointer - transform.position);
            }
        }

        public void BackToDefaultPosition()
        {
            for (int i = 0; i < tileViews.Count; i++)
            {
                tileViews[i].SetDefaultPosition();
                tileViews[i].ApplyScale(GameViewConfigs.BLOCK_SCALE);
            }
        }

        private void SetUpView(int[] viewData)
        {
            float halfCol = GameConfigs.BLOCK_COL_COUNT / 2f;
            float halfRow = GameConfigs.BLOCK_ROW_COUNT / 2f;

            for (int i = 0; i < viewData.Length; i++)
            {
                if (viewData[i] != 0)
                {
                    var tileView = PoolController.GetTileView();
                    int currentCol = i % GameConfigs.BLOCK_COL_COUNT;
                    int currentRow = i / GameConfigs.BLOCK_COL_COUNT;

                    Vector3 viewPosition = new Vector3(
                        (currentCol - halfCol + 0.5f) * (GameViewConfigs.TILE_SIZE + GameViewConfigs.TILE_GAP),
                        (halfRow - currentRow - 0.5f) * (GameViewConfigs.TILE_SIZE + GameViewConfigs.TILE_GAP)
                    );
                    tileView.SetPosition(viewPosition, transform.position);
                    tileView.UpdateTileLevel(viewData[i]);
                    tileViews.Add(tileView);
                }
            }

            NormalizeView();
            ApplyBlockScale(GameViewConfigs.BLOCK_SCALE);
        }

        private void ApplyBlockScale(float scale)
        {
            for (int i = 0; i < tileViews.Count; i++)
            {
                tileViews[i].ApplyScale(scale);
            }
        }

        private void NormalizeView()
        {
            float maxY = -Mathf.Infinity;
            float minY = Mathf.Infinity;

            float maxX = -Mathf.Infinity;
            float minX = Mathf.Infinity;

            for (int i = 0; i < tileViews.Count; i++)
            {
                var currentView = tileViews[i].GetPosition();

                if (currentView.y > maxY)
                {
                    maxY = currentView.y;
                }

                if (currentView.y < minY)
                {
                    minY = currentView.y;
                }

                if (currentView.x > maxX)
                {
                    maxX = currentView.x;
                }

                if (currentView.x < minX)
                {
                    minX = currentView.x;
                }
            }

            float middleY = (maxY + minY) / 2;
            float middleX = (maxX + minX) / 2;

            var translation = new Vector3(-middleX, -middleY) + transform.position;

            for (int i = 0; i < tileViews.Count; i++)
            {
                tileViews[i].Translate(translation);
            }
        }
    }
}