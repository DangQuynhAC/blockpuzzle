﻿using System.Collections;
using System.Collections.Generic;
using BPCore;
using BPLogic;
using BPPools;
using UnityEngine;

namespace BPViews
{
    public class BoardView : MonoBehaviour
    {
        private CellView[] cellViews;

        public void Init()
        {
            cellViews = new CellView[GameConfigs.BOARD_COL_COUNT * GameConfigs.BOARD_ROW_COUNT];

            float halfCol = GameConfigs.BOARD_COL_COUNT / 2f;
            float halfRow = GameConfigs.BOARD_ROW_COUNT / 2f;

            for (int i = 0; i < cellViews.Length; i++)
            {
                cellViews[i] = PoolController.GetCellView();
                int currentCol = i % GameConfigs.BOARD_COL_COUNT;
                int currentRow = i / GameConfigs.BOARD_COL_COUNT;

                Vector3 viewPosition = new Vector3(
                    (currentCol - halfCol + 0.5f) * (GameViewConfigs.TILE_SIZE + GameViewConfigs.TILE_GAP),
                    (halfRow - currentRow - 0.5f) * (GameViewConfigs.TILE_SIZE + GameViewConfigs.TILE_GAP)
                );

                cellViews[i].SetPosition(viewPosition + transform.position);
            }
        }

        public void ResetView()
        {
            for (int i = 0; i < cellViews.Length; i++)
            {
                cellViews[i].RemoveView();
            }
        }

        public void AddTileView(TileView view)
        {
            cellViews[WorldToBoardIndex(view.GetPosition())].SetView(view);
        }

        public int WorldToBoardIndex(Vector3 position)
        {
            position -= transform.position;

            float halfCol = GameConfigs.BOARD_COL_COUNT / 2f;
            float halfRow = GameConfigs.BOARD_ROW_COUNT / 2f;

            float length = GameViewConfigs.TILE_SIZE + GameViewConfigs.TILE_GAP;

            int col = Mathf.RoundToInt(position.x / length - 0.5f + halfCol);
            int row = Mathf.RoundToInt(halfRow - position.y / length - 0.5f);

            return row * GameConfigs.BOARD_COL_COUNT + col;
        }

        public int WorldToBoardIndexCol(Vector3 position)
        {
            var temp = position - transform.position;

            float halfCol = GameConfigs.BOARD_COL_COUNT / 2f;
            float halfRow = GameConfigs.BOARD_ROW_COUNT / 2f;

            float length = GameViewConfigs.TILE_SIZE + GameViewConfigs.TILE_GAP;

            int col = Mathf.RoundToInt(temp.x / length - 0.5f + halfCol);

            return col;
        }

        public int WorldToBoardIndexRow(Vector3 position)
        {
            var temp = position - transform.position;

            float halfCol = GameConfigs.BOARD_COL_COUNT / 2f;
            float halfRow = GameConfigs.BOARD_ROW_COUNT / 2f;

            float length = GameViewConfigs.TILE_SIZE + GameViewConfigs.TILE_GAP;

            int row = Mathf.RoundToInt(halfRow - temp.y / length - 0.5f);

            return row;
        }

        public bool IsValidIndex(int col, int row)
        {
            return col >= 0 && col < GameConfigs.BOARD_COL_COUNT && row >= 0 && row < GameConfigs.BOARD_ROW_COUNT;
        }


        public void DeEmphasizeBoard()
        {
            for (int i = 0; i < cellViews.Length; i++)
            {
                cellViews[i].DeEmphasize();
            }
        }

        public void HighlightCell(int index)
        {
            cellViews[index].Highlight();
        }

        public void HandleRemoveTiles(List<int> indexesToRemove)
        {
            for (int i = 0; i < indexesToRemove.Count; i++)
            {
                cellViews[indexesToRemove[i]].RemoveView();
            }
        }

        public void HandleUpdateView(int[] board)
        {
            for (int i = 0; i < cellViews.Length; i++)
            {
                if (board[i] != 0)
                {
                    if (cellViews[i].CellReady)
                    {
                        cellViews[i].UpdateView(board[i]);
                    }
                    else
                    {
                        var view = PoolController.GetTileView();
                        view.UpdateTileLevel(board[i]);
                        cellViews[i].SetView(view);
                    }
                }
                else
                {
                    cellViews[i].RemoveView();
                }
            }
        }
    }
}