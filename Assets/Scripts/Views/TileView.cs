﻿using System.Collections;
using System.Collections.Generic;
using BPPools;
using UnityEngine;
using TMPro;
using Unity.Mathematics;

namespace BPViews
{
    public class TileView : MonoBehaviour, IPoolable
    {
        [SerializeField] private TextMeshProUGUI textTileLevel;

        private Vector3 defaultPosition;
        private Vector3 rootPosition;
        private float currentScale = 1f;

        [SerializeField] private List<Color> colors;

        public Vector3 GetPosition()
        {
            return transform.position;
        }

        public void SetDefaultPosition()
        {
            transform.position = defaultPosition * currentScale + rootPosition;
        }

        public void SetPosition(Vector3 position, Vector3 root)
        {
            defaultPosition = position / currentScale;
            rootPosition = root;

            transform.position = defaultPosition + rootPosition;
        }

        public void SetPosition(Vector3 position)
        {
            defaultPosition = (position - rootPosition) / currentScale;
            transform.position = position;
        }

        public void SetDelta(Vector3 deltaVector)
        {
            transform.position += deltaVector;
        }

        public void Translate(Vector3 translation)
        {
            defaultPosition += translation / currentScale;
            transform.position += translation;
        }

        public void ApplyScale(float scale)
        {
            transform.position = defaultPosition * scale + rootPosition;
            transform.localScale = new Vector3(scale, scale);

            currentScale = scale;
        }

        public void UpdateTileLevel(int level)
        {
            UpdateLevelText(level);
        }

        private void UpdateLevelText(int level)
        {
            textTileLevel.text = level > 1 ? (level-1).ToString() : "";

            var sR = GetComponent<SpriteRenderer>();
            sR.color = colors[Mathf.Min(level - 1, colors.Count - 1)];
        }

        public void Active()
        {
            gameObject.SetActive(true);
        }

        public void Dispose()
        {
            defaultPosition = Vector3.zero;
            rootPosition = Vector3.zero;
            currentScale = 1f;

            textTileLevel.text = "";
            gameObject.SetActive(false);
        }
    }
}