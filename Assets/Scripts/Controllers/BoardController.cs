﻿using System;
using System.Collections;
using System.Collections.Generic;
using BPLogic;
using BPViews;
using UnityEngine;

namespace Controllers
{
    [RequireComponent(typeof(BoardView))]
    public class BoardController : MonoBehaviour
    {
        public event Action<List<int>> onDroppingBlockIntoBoard = ints => { };

        public delegate bool EmptyTileChecker(int index);

        public event EmptyTileChecker emptyTileChecker = i => false;

        private BoardView boardView;
        private List<int> validIndexes;

        public void Init()
        {
            boardView = GetComponent<BoardView>();
            validIndexes = new List<int>();
        }

        public void ResetController()
        {
            
        }

        public void HandleBlockMoving(List<TileView> views)
        {
            boardView.DeEmphasizeBoard();

            for (int i = 0; i < views.Count; i++)
            {
                int boardIndexCol = boardView.WorldToBoardIndexCol(views[i].GetPosition());
                int boardIndexRow = boardView.WorldToBoardIndexRow(views[i].GetPosition());

                int boardIndex = boardView.WorldToBoardIndex(views[i].GetPosition());

                if (!boardView.IsValidIndex(boardIndexCol, boardIndexRow) || !emptyTileChecker(boardIndex))
                {
                    boardView.DeEmphasizeBoard();
                    return;
                }

                boardView.HighlightCell(boardIndex);
            }
        }

        public void HandleBlockStopping(List<TileView> views, Action onValid, Action onInvalid)
        {
            boardView.DeEmphasizeBoard();
            validIndexes.Clear();
            
            if (IsBlockPointingToValidIndex(views))
            {
                for (int i = 0; i < views.Count; i++)
                {
                    validIndexes.Add(boardView.WorldToBoardIndex(views[i].GetPosition()));
                    boardView.AddTileView(views[i]);
                }

                OrganizeIndexList();
                onValid();
                
                onDroppingBlockIntoBoard(validIndexes);
            }
            else
            {
                onInvalid();
            }
        }

        public void HandleRemoveTiles(List<int> indexesToRemove)
        {
            boardView.HandleRemoveTiles(indexesToRemove);
        }

        public void HandleUpdateView(int[] board)
        {
            boardView.HandleUpdateView(board);
        }

        private void OrganizeIndexList()
        {
            validIndexes.Sort(IndexComparer);
        }

        private int IndexComparer(int index1, int index2)
        {
            int col1 = index1 % GameConfigs.BLOCK_COL_COUNT;
            int row1 = index1 / GameConfigs.BLOCK_COL_COUNT;

            int col2 = index2 % GameConfigs.BLOCK_COL_COUNT;
            int row2 = index2 / GameConfigs.BLOCK_COL_COUNT;

            if (row1 < row2)
            {
                return -1;
            }

            if (row1 > row2)
            {
                return 1;
            }

            if (col1 < col2)
            {
                return -1;
            }

            return 1;
        }

        private bool IsBlockPointingToValidIndex(List<TileView> views)
        {
            for (int i = 0; i < views.Count; i++)
            {
                Vector3 currentPos = views[i].GetPosition();
                int boardIndexCol = boardView.WorldToBoardIndexCol(currentPos);
                int boardIndexRow = boardView.WorldToBoardIndexRow(currentPos);

                int boardIndex = boardView.WorldToBoardIndex(currentPos);

                if (!boardView.IsValidIndex(boardIndexCol, boardIndexRow) || !emptyTileChecker(boardIndex))
                {
                    return false;
                }
            }

            return true;
        }
    }
}