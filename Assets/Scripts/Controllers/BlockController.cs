﻿using System;
using System.Collections.Generic;
using Base;
using BPViews;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BPController
{
    [RequireComponent(typeof(BlockView))]
    public class BlockController : BaseDraggable
    {
        public event Action<int> onBlockRotating = i => { };
        public event Action<int> onBlockBeginMoving = i => { };
        public event Action<List<TileView>> onBlockMoving = list => { };
        public event Action<List<TileView>, Action, Action> onBlockStopping = (list, action, arg3) => { };

        private BlockView blockView;

        private int index;
        
        public override void Init()
        {
            blockView = GetComponent<BlockView>();
            hitbox = GetComponent<BoxCollider2D>();
            mainCamera = Camera.main;
        }

        public void ResetController()
        {
        }

        public void SetIndex(int index)
        {
            this.index = index;
        }

        public void SetData(int[] data)
        {
            blockView.UpdateView(data);
        }

        public override void OnBeginDrag(PointerEventData eventData)
        {
            hitbox.enabled = false;
            var pointer = mainCamera.ScreenToWorldPoint(eventData.position);
            blockView.SetMovingPosition(pointer);
            onBlockBeginMoving(index);
        }

        public override void OnDrag(PointerEventData eventData)
        {
            var pointer = mainCamera.ScreenToWorldPoint(eventData.position);
            blockView.SetMovingPosition(pointer);
            onBlockMoving(blockView.GetViews());
        }

        public override void OnEndDrag(PointerEventData eventData)
        {
            onBlockStopping(blockView.GetViews(), OnDropValidIndex, OnDropInvalidIndex);
            hitbox.enabled = true;
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            blockView.RotateView();
            onBlockRotating(index);
        }

        private void OnDropValidIndex()
        {
            blockView.ClearView();
        }

        private void OnDropInvalidIndex()
        {
            blockView.BackToDefaultPosition();
        }
    }
}