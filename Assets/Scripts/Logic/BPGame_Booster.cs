﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace BPLogic
{
    public partial class BPGame
    {
        public void ApplyBoosterBoom(Vector2Int atIndex)
        {
            RemoveValidIndex(atIndex);
            RemoveValidIndex(atIndex.Left());
            RemoveValidIndex(atIndex.Right());
            RemoveValidIndex(atIndex.Up());
            RemoveValidIndex(atIndex.Down());
            RemoveValidIndex(atIndex.Up().Left());
            RemoveValidIndex(atIndex.Up().Right());
            RemoveValidIndex(atIndex.Down().Left());
            RemoveValidIndex(atIndex.Down().Right());

            onBoardUpdate(board.Data);
        }

        private void RemoveValidIndex(Vector2Int index)
        {
            if (board.IsValidIndex(index))
            {
                board.RemoveValue(index);
            }
        }
    }
}