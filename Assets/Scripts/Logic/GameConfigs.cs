﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BPLogic
{
    public class GameConfigs
    {
        public const int BOARD_ROW_COUNT = 6;
        public const int BOARD_COL_COUNT = 6;

        public const int BLOCK_ROW_COUNT = 4;
        public const int BLOCK_COL_COUNT = 4;

        public static int MAX_LEVEL = 4;

        public static List<BPBlockLevelConfig> BLOCK_LEVEL_CONFIGS = new List<BPBlockLevelConfig>();

        public static List<BPBlockTemplateConfig> BLOCK_TEMPLATE_CONFIGS = new List<BPBlockTemplateConfig>();

        public const int NUMBER_OF_BLOCK = 3;

        public const int TEMPLATE_TYPE_TOBE_CLEAN = 4;

        public const int COLLECTIBLES_REQUIRED_START = 10;

        public const int TROPHIES_GAINED_START = 50;

        public const float COLLECTIBLES_REQUIRED_MULTIPLIER = 1.5f;

        public const float TROPHIES_GAINED_MULTIPLIER = 1.75f;
    }

    public class ScoreConfigs
    {
        public const int BASE_POINT = 10;
        public const float COMBO_MULTIPLIER_2 = 1.5f;
        public const float COMBO_MULTIPLIER_3 = 2f;
        public const float COMBO_MULTIPLIER_4 = 2.5f;
        public const float COMBO_MULTIPLIER_5 = 3f;
    }

    public class GameViewConfigs
    {
        public const float TILE_SIZE = 1f;
        public const float TILE_GAP = 0.1f;

        public const float BLOCK_SCALE = 0.5f;
        public const float BLOCK_DRAGGING_SCALE = 1f;

        public const float MOVING_OFFSET = 1.5f;
    }
}