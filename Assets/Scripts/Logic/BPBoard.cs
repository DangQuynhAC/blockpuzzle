﻿using System.Collections;
using System.Collections.Generic;
using MessagePack;
using UnityEngine;

namespace BPLogic
{
    [MessagePackObject]
    public class BPBoard
    {
        [Key(0)] public int[] data;

        [IgnoreMember] public int[] Data => data;

        [Key(1)] public int[] maskData;

        private int size => data.Length;

        private List<int> collectibleTiles;

        [IgnoreMember]
        public List<int> CollectibleTiles
        {
            get
            {
                if (collectibleTiles == null)
                {
                    collectibleTiles = new List<int>();
                }

                collectibleTiles.Clear();

                for (int i = 0; i < size; i++)
                {
                    if (data[i] > 1 && maskData[i] > 0)
                    {
                        collectibleTiles.Add(i);
                    }
                }

                return collectibleTiles;
            }
        }

        public BPBoard(int[] data, int[] maskData)
        {
            this.data = data;
            this.maskData = maskData;
        }

        public BPBoard(int size)
        {
            data = new int[size];
            for (int i = 0; i < size; i++)
            {
                data[i] = 0;
            }
        }

        public BPBoard()
        {
        }

        public void ResetMask()
        {
            if (maskData == null)
            {
                maskData = new int[data.Length];
            }

            for (int i = 0; i < maskData.Length; i++)
            {
                maskData[i] = 0;
            }
        }

        public void ApplyMask()
        {
            for (int i = 0; i < data.Length; i++)
            {
                data[i] -= maskData[i];
                data[i] = data[i] < 0 ? 0 : data[i];
            }
        }

        public void SetMaskValueAt(int value, int index)
        {
            maskData[index] = Mathf.Max(maskData[index], value);
        }

        public bool IsValidIndex(Vector2Int index)
        {
            return index.x >= 0 && index.x < GameConfigs.BOARD_COL_COUNT && index.y >= 0 &&
                   index.y < GameConfigs.BOARD_ROW_COUNT;
        }

        public void WriteValue(int value, int index)
        {
            data[index] = value;
        }

        public void RemoveValue(int index)
        {
            data[index] = 0;
        }

        public void RemoveValue(Vector2Int index)
        {
            data[index.x + index.y * GameConfigs.BOARD_COL_COUNT] = 0;
        }

        public int ValueAt(int index)
        {
            return data[index];
        }

        public bool IsEmptyAt(Vector2Int index)
        {
            return data[index.x + index.y * GameConfigs.BOARD_COL_COUNT] == 0;
        }

        public bool IsEmptyAt(int index)
        {
            return data[index] == 0;
        }
    }
}