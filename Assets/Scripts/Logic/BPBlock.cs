﻿using System;
using System.Collections;
using System.Collections.Generic;
using MessagePack;
using UnityEngine;
using Utils;
using Random = UnityEngine.Random;

namespace BPLogic
{
    [MessagePackObject]
    public class BPBlock
    {
        [Key(0)] public int TilesCount { get; }

        [Key(1)] public readonly int[] data;

        [IgnoreMember] public int[] Data => data;

        private List<int> meaningfulIndexes;

        private List<int> visitedIndexes;

        [Key(2)] public BlockType type;

        private BPBlockPlayableRules.PlayableRule playableRules;

        [Key(3)] public bool isAvailable;

        [IgnoreMember] public BPBlockPlayableRules.PlayableRule PlayableRules => playableRules;

        [IgnoreMember]
        public bool MeaningfulIndexesAvailableForSettingLevel
        {
            get
            {
                if (meaningfulIndexes == null || meaningfulIndexes.Count == 0)
                {
                    return false;
                }

                return true;
            }
        }

        [IgnoreMember] public bool IsAvailable => isAvailable;

        [SerializationConstructor]
        public BPBlock(int tilesCount, int[] data, BlockType type, bool isAvailable = true)
        {
            TilesCount = tilesCount;

            this.data = new int[data.Length];
            Array.Copy(data, this.data, data.Length);

            this.type = type;

            this.playableRules = BPBlockPlayableRules.GetPlayableRulesByBlockType(this.type);

            this.isAvailable = isAvailable;

            meaningfulIndexes = new List<int>();
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] != 0)
                {
                    meaningfulIndexes.Add(i);
                }
            }

            visitedIndexes = new List<int>();
        }

        public bool SetLevelRandomly(int level)
        {
            if (meaningfulIndexes.Count == 0)
            {
                return false;
            }

            if (visitedIndexes.Count != 0)
            {
                visitedIndexes.Shuffle();

                for (int i = 0; i < visitedIndexes.Count; i++)
                {
                    List<int> neighborhood = Neighborhood(visitedIndexes[i]);

                    while (neighborhood.Count > 0)
                    {
                        int destination = neighborhood[Random.Range(0, 1000) % neighborhood.Count];
                        if (!visitedIndexes.Contains(destination))
                        {
                            data[destination] = level;

                            visitedIndexes.Add(destination);
                            meaningfulIndexes.Remove(destination);

                            return true;
                        }

                        neighborhood.Remove(destination);
                    }
                }

                return false;
            }

            meaningfulIndexes.Shuffle();

            int current = Random.Range(0, 1000) % meaningfulIndexes.Count;
            data[meaningfulIndexes[current]] = level;

            visitedIndexes.Add(meaningfulIndexes[current]);

            meaningfulIndexes.RemoveAt(current);

            return true;
        }

        public bool IsEmptyAt(int index)
        {
            return data[index] == 0;
        }

        public int ValueAt(int index)
        {
            return data[index];
        }

        public void SetAvailability(bool availability)
        {
            isAvailable = availability;
        }

        public void Rotate()
        {
            for (int x = 0; x < GameConfigs.BLOCK_COL_COUNT / 2; x++)
            {
                for (int y = x; y < GameConfigs.BLOCK_COL_COUNT - 1 - x; y++)
                {
                    int topIndex = x * GameConfigs.BLOCK_COL_COUNT + y;
                    int leftIndex = (GameConfigs.BLOCK_COL_COUNT - 1 - y) * GameConfigs.BLOCK_COL_COUNT + x;
                    int bottomIndex = (GameConfigs.BLOCK_COL_COUNT - 1 - x) * GameConfigs.BLOCK_COL_COUNT +
                                      (GameConfigs.BLOCK_COL_COUNT - 1 - y);
                    int rightIndex = y * GameConfigs.BLOCK_COL_COUNT + GameConfigs.BLOCK_COL_COUNT - 1 - x;

                    int temp = data[topIndex];

                    data[topIndex] = data[leftIndex];
                    data[leftIndex] = data[bottomIndex];
                    data[bottomIndex] = data[rightIndex];
                    data[rightIndex] = temp;
                }
            }
        }

        private List<int> Neighborhood(int i)
        {
            List<int> neighbors = new List<int>();

            int col = i % GameConfigs.BLOCK_COL_COUNT;
            int row = i / GameConfigs.BLOCK_ROW_COUNT;

            if (CheckIndexValidity(col - 1, row))
            {
                neighbors.Add(col - 1 + row * GameConfigs.BLOCK_COL_COUNT);
            }

            if (CheckIndexValidity(col + 1, row))
            {
                neighbors.Add(col + 1 + row * GameConfigs.BLOCK_COL_COUNT);
            }

            if (CheckIndexValidity(col, row - 1))
            {
                neighbors.Add(col + (row - 1) * GameConfigs.BLOCK_COL_COUNT);
            }

            if (CheckIndexValidity(col, row + 1))
            {
                neighbors.Add(col + (row + 1) * GameConfigs.BLOCK_COL_COUNT);
            }

            return neighbors;
        }

        private bool CheckIndexValidity(int col, int row)
        {
            return col >= 0 && col < GameConfigs.BLOCK_COL_COUNT && row >= 0 && row < GameConfigs.BLOCK_COL_COUNT &&
                   meaningfulIndexes.Contains(col + row * GameConfigs.BLOCK_COL_COUNT);
        }
    }
}