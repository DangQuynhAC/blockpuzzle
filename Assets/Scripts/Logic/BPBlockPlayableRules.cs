﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace BPLogic
{
    public static class BPBlockPlayableRules
    {
        public delegate List<Vector2Int[]> PlayableRule(Vector2Int index);

        public static PlayableRule GetPlayableRulesByBlockType(BlockType type)
        {
            switch (type)
            {
                case BlockType.Square1x1:
                    return PlayableRulesFor1x1;

                case BlockType.Line1x2:
                    return PlayableRulesForLine1x2;

                case BlockType.Line1x3:
                    return PlayableRulesForLine1x3;

                case BlockType.Line1x4:
                    return PlayableRulesForLine1x4;

                case BlockType.Square2x2:
                    return PlayableRulesForSquare2x2;

                case BlockType.LShape2x2:
                    return PlayableRulesForL2x2;

                case BlockType.LShape2x3:
                    return PlayableRulesForL2x3;

                case BlockType.LShape2x3_2:
                    return PlayableRulesForL2x3_2;

                case BlockType.TShape2x3:
                    return PlayableRulesForT2x3;

                case BlockType.ZShape2x3:
                    return PlayableRulesForZ2x3;

                case BlockType.ZShape2x3_R:
                    return PlayableRulesForZ2x3_R;

                default:
                    return null;
            }
        }

        private static List<Vector2Int[]> PlayableRulesFor1x1(Vector2Int index)
        {
            return new List<Vector2Int[]>
            {
                new[] {index}
            };
        }

        private static List<Vector2Int[]> PlayableRulesForLine1x2(Vector2Int index)
        {
            return new List<Vector2Int[]>
            {
                new[] {index, index.OffsetX(1)},
                new[] {index, index.OffsetY(1)}
            };
        }

        private static List<Vector2Int[]> PlayableRulesForLine1x3(Vector2Int index)
        {
            return new List<Vector2Int[]>
            {
                new[] {index, index.OffsetX(1), index.OffsetX(2)},
                new[] {index, index.OffsetY(1), index.OffsetY(2)}
            };
        }

        private static List<Vector2Int[]> PlayableRulesForLine1x4(Vector2Int index)
        {
            return new List<Vector2Int[]>
            {
                new[] {index, index.OffsetX(1), index.OffsetX(2), index.OffsetX(3)},
                new[] {index, index.OffsetY(1), index.OffsetY(2), index.OffsetY(3)}
            };
        }

        private static List<Vector2Int[]> PlayableRulesForSquare2x2(Vector2Int index)
        {
            return new List<Vector2Int[]>
            {
                new[] {index, index.OffsetX(1), index.OffsetY(1), index.OffsetY(1).OffsetX(1)}
            };
        }

        private static List<Vector2Int[]> PlayableRulesForL2x2(Vector2Int index)
        {
            return new List<Vector2Int[]>
            {
                new[] {index, index.OffsetY(1), index.OffsetY(1).OffsetX(1)},
                new[] {index, index.OffsetX(1), index.OffsetY(1)},
                new[] {index, index.OffsetX(1), index.OffsetY(1).OffsetX(1)},
                new[] {index, index.OffsetY(1).OffsetX(-1), index.OffsetY(1)}
            };
        }

        private static List<Vector2Int[]> PlayableRulesForL2x3(Vector2Int index)
        {
            return new List<Vector2Int[]>
            {
                new[] {index, index.OffsetY(1).OffsetX(-2), index.OffsetY(1).OffsetX(-1), index.OffsetY(1)},
                new[] {index, index.OffsetY(1), index.OffsetY(2), index.OffsetY(2).OffsetX(1)},
                new[] {index, index.OffsetX(1), index.OffsetX(2), index.OffsetY(1)},
                new[] {index, index.OffsetX(1), index.OffsetY(1).OffsetX(1), index.OffsetY(2).OffsetX(1)}
            };
        }

        private static List<Vector2Int[]> PlayableRulesForL2x3_2(Vector2Int index)
        {
            return new List<Vector2Int[]>
            {
                new[] {index, index.OffsetY(1), index.OffsetY(2), index.OffsetY(2).OffsetX(-1)},
                new[] {index, index.OffsetY(1), index.OffsetY(1).OffsetX(1), index.OffsetY(1).OffsetX(2)},
                new[] {index, index.OffsetX(1), index.OffsetY(1), index.OffsetY(2)},
                new[] {index, index.OffsetX(1), index.OffsetX(2), index.OffsetY(1).OffsetX(2)}
            };
        }

        private static List<Vector2Int[]> PlayableRulesForT2x3(Vector2Int index)
        {
            return new List<Vector2Int[]>
            {
                new[] {index, index.OffsetY(1).OffsetX(-1), index.OffsetY(1), index.OffsetY(1).OffsetX(1)},
                new[] {index, index.OffsetY(1), index.OffsetY(1).OffsetX(1), index.OffsetY(2)},
                new[] {index, index.OffsetX(1), index.OffsetX(2), index.OffsetY(1).OffsetX(1)},
                new[] {index, index.OffsetY(1).OffsetX(-1), index.OffsetY(1), index.OffsetY(2)}
            };
        }

        private static List<Vector2Int[]> PlayableRulesForZ2x3(Vector2Int index)
        {
            return new List<Vector2Int[]>()
            {
                new[] {index, index.OffsetX(1), index.OffsetY(1).OffsetX(1), index.OffsetY(1).OffsetX(2)},
                new[] {index, index.OffsetY(1).OffsetX(-1), index.OffsetY(1), index.OffsetY(2).OffsetX(-1)}
            };
        }

        private static List<Vector2Int[]> PlayableRulesForZ2x3_R(Vector2Int index)
        {
            return new List<Vector2Int[]>()
            {
                new[] {index, index.OffsetX(1), index.OffsetY(1).OffsetX(-1), index.OffsetY(1)},
                new[] {index, index.OffsetY(1), index.OffsetY(1).OffsetX(1), index.OffsetY(2).OffsetX(1)}
            };
        }
    }
}