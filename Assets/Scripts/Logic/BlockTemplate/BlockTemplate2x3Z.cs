﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace BPLogic
{
    public class BlockTemplate2x3Z : IBPBlockTemplate
    {
        public BlockType Type => BlockType.ZShape2x3;
        
        public int TileCount => 4;
        public int[] Data => template;

        private int[] template =
        {
            1, 1, 0, 0,
            0, 1, 1, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
        };
    }
}