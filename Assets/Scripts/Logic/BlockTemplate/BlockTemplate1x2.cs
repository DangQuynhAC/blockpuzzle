﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace BPLogic
{
    public class BlockTemplate1x2 : IBPBlockTemplate
    {
        public BlockType Type => BlockType.Line1x2;
        public int TileCount => 2; 

        public int[] Data => template;

        private int[] template =
        {
            1, 1, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
        };
    }
}