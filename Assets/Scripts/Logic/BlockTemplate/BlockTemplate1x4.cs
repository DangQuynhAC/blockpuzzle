﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace BPLogic
{
    public class BlockTemplate1x4 : IBPBlockTemplate
    {
        public BlockType Type => BlockType.Line1x4;
        
        public int TileCount => 4;
        
        public int[] Data => template;

        private int[] template =
        {
            1, 1, 1, 1,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
        };
    }
}