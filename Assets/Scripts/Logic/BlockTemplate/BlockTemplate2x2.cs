﻿using System.Collections;
using System.Collections.Generic;
using BPLogic;
using UnityEngine;
using Utils;

namespace BPLogic
{
    public class BlockTemplate2x2 : IBPBlockTemplate
    {
        public BlockType Type => BlockType.Square2x2;
        
        public int TileCount => 4;
        public int[] Data => data;

        private int[] data = {
            1, 1, 0, 0,
            1, 1, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
        };
    }
}