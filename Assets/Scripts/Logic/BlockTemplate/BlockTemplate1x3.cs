﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace BPLogic
{
    public class BlockTemplate1x3 : IBPBlockTemplate
    {
        public BlockType Type => BlockType.Line1x3;
        public int TileCount => 3;

        public int[] Data => template;

        private int[] template =
        {
            1, 1, 1, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
        };
    }
}