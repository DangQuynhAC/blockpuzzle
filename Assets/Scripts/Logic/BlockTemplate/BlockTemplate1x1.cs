﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BPLogic
{
    public class BlockTemplate1x1 : IBPBlockTemplate
    {
        public BlockType Type => BlockType.Square1x1;
        
        public int TileCount => 1; 

        public int[] Data => template;

        private int[] template =
        {
            1, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
        };
    }
}