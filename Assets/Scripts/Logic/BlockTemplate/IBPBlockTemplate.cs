﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BPLogic
{
    public interface IBPBlockTemplate
    {
        BlockType Type { get; }

        int TileCount { get; }

        int[] Data { get; }
    }


    public enum BlockType
    {
        Square1x1,
        Line1x2,
        Line1x3,
        Line1x4,
        Square2x2,
        LShape2x2,
        LShape2x3,
        LShape2x3_2,
        TShape2x3,
        ZShape2x3,
        ZShape2x3_R
    }
}