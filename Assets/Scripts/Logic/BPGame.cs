﻿using System;
using System.Collections.Generic;
using Configs;
using MessagePack;
using UnityEngine;

namespace BPLogic
{
    [MessagePackObject]
    public partial class BPGame
    {
        public event Action<int, int[]> onGenerateNewBlock = (i, ints) => { };
        public event Action<int[]> onBoardUpdate = ints => { };
        public event Action onGameOver = () => { };
        public event Action<int> onClearLines = i => { };
        public event Action onPrepareToGenerateNewBlock = () => { };
        public event Action<int> onPutBlock = i => { };
        public event Action<List<int>> onCollectTiles = ints => { };

        [Key(0)] public BPBlock[] blocks;

        [Key(1)] public BPBoard board;

        [Key(2)] public BPBlockGenerator blockGenerator;

        [Key(3)] public BPBlockLevelGenerator blockLevelGenerator;

        private List<int> fullRows;
        private List<int> fullCols;

        private List<int> cleanedIndexes;
        private List<int> indexesToRemove;

        private int clearLinesCount;

        [SerializationConstructor]
        public BPGame(BPBlock[] blocks, BPBoard board, BPBlockGenerator blockGenerator,
            BPBlockLevelGenerator blockLevelGenerator)
        {
            this.blocks = blocks;
            this.board = board;
            this.blockGenerator = blockGenerator;
            this.blockLevelGenerator = blockLevelGenerator;

            Debug.Log("deserialize bpgame");

            fullRows = new List<int>();
            fullCols = new List<int>();
            indexesToRemove = new List<int>();

            cleanedIndexes = new List<int>();
        }

        public BPGame()
        {
            blocks = new BPBlock[3];
            board = new BPBoard(GameConfigs.BOARD_COL_COUNT * GameConfigs.BOARD_ROW_COUNT);
            blockGenerator = new BPBlockGenerator();
            blockLevelGenerator = new BPBlockLevelGenerator();

            fullRows = new List<int>();
            fullCols = new List<int>();
            indexesToRemove = new List<int>();

            cleanedIndexes = new List<int>();
        }

        public void Start()
        {
            GenerateNewBlock();
            // StartFromSaveData();
        }

        public void StartFromSaveData()
        {
            onBoardUpdate(board.Data);

            for (int i = 0; i < blocks.Length; i++)
            {
                if (blocks[i].IsAvailable)
                {
                    onGenerateNewBlock(i, blocks[i].Data);
                }
            }
            
            OnGameUpdate();
        }

        public void Reset()
        {
            blocks = new BPBlock[3];
            board = new BPBoard(GameConfigs.BOARD_COL_COUNT * GameConfigs.BOARD_ROW_COUNT);
            blockGenerator = new BPBlockGenerator();

            fullRows.Clear();
            fullCols.Clear();
            indexesToRemove.Clear();

            GenerateNewBlock();
        }

        public void SetScoreBasedConfig(ScoreBasedConfig scoreBasedConfig)
        {
            blockLevelGenerator.SetLevelConfig(scoreBasedConfig.levelConfig);
            blockGenerator.SetBlockConfig(scoreBasedConfig.blockConfig);
        }

        public void PutBlock(int blockIndex, List<int> validIndexes)
        {
            BPBlock currentBlock = blocks[blockIndex];

            int currentNonEmptySlot = 0;
            for (int i = 0; i < GameConfigs.BLOCK_COL_COUNT * GameConfigs.BLOCK_ROW_COUNT; i++)
            {
                if (!currentBlock.IsEmptyAt(i))
                {
                    board.WriteValue(currentBlock.ValueAt(i), validIndexes[currentNonEmptySlot]);
                    currentNonEmptySlot++;
                }
            }

            onPutBlock(currentBlock.TilesCount);

            currentBlock.SetAvailability(false);

            OnGameUpdate();
        }

        public void RotateBlock(int blockId)
        {
            blocks[blockId].Rotate();
        }

        public bool IsEmptyAt(int index)
        {
            return board.IsEmptyAt(index);
        }

        private void GenerateNewBlock()
        {
            onPrepareToGenerateNewBlock();

            // IBPBlockTemplate temp1 = new BlockTemplate2x2L();
            //
            // blocks[0] = new BPBlock(temp1.TileCount, temp1.Data, temp1.Type);
            // blocks[1] = new BPBlock(temp1.TileCount, temp1.Data, temp1.Type);
            // blocks[2] = new BPBlock(temp1.TileCount, temp1.Data, temp1.Type);

            for (int i = 0; i < blocks.Length; i++)
            {
                IBPBlockTemplate template = blockGenerator.GetRandomTemplate();
                blocks[i] = new BPBlock(template.TileCount, template.Data, template.Type);
            }
            
            blockLevelGenerator.SetLevelsForBlocks(blocks);
            
            for (int i = 0; i < blocks.Length; i++)
            {
                onGenerateNewBlock(i, blocks[i].Data);
            }
        }

        private void OnGameUpdate()
        {
            CheckFullLines();
            CleanFullLines();
    
            if (ShouldGenerateNewBlocks())
            {
                GenerateNewBlock();
            }

            CheckGameOver();
        }

        private void CheckGameOver()
        {
            if (!IsBlockPlayable(blocks[0]) && !IsBlockPlayable(blocks[1]) && !IsBlockPlayable(blocks[2]))
            {
                onGameOver();
            }
        }

        private bool IsBlockPlayable(BPBlock block)
        {
            if (!block.IsAvailable)
            {
                return false;
            }

            for (int i = 0; i < GameConfigs.BOARD_COL_COUNT * GameConfigs.BOARD_ROW_COUNT; i++)
            {
                Vector2Int currentBoardIndex = Vector2Int.zero;

                currentBoardIndex.x = i % GameConfigs.BOARD_COL_COUNT;
                currentBoardIndex.y = i / GameConfigs.BOARD_COL_COUNT;

                var playableRules = block.PlayableRules(currentBoardIndex);
                for (int j = 0; j < playableRules.Count; j++)
                {
                    int playableIndexesCount = 0;

                    for (int k = 0; k < playableRules[j].Length; k++)
                    {
                        Vector2Int index = playableRules[j][k];
                        if (board.IsValidIndex(index) && board.IsEmptyAt(index))
                        {
                            playableIndexesCount++;
                        }
                    }

                    if (playableIndexesCount == playableRules[j].Length)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool ShouldGenerateNewBlocks()
        {
            for (int i = 0; i < blocks.Length; i++)
            {
                if (blocks[i].IsAvailable)
                {
                    return false;
                }
            }

            return true;
        }

        private void CheckFullLines()
        {
            fullRows.Clear();
            fullCols.Clear();
            indexesToRemove.Clear();

            CheckFullCols();
            CheckFullRows();
        }

        private void CleanFullLines()
        {
            cleanedIndexes.Clear();
            board.ResetMask();
            clearLinesCount = 0;

            CleanFullCols();
            CleanFullRows();

            onCollectTiles(board.CollectibleTiles);

            board.ApplyMask();

            onClearLines(clearLinesCount);

            onBoardUpdate(board.Data);
        }

        private void CleanFullCols()
        {
            for (int i = 0; i < fullCols.Count; i++)
            {
                int colMinValue = board.ValueAt(fullCols[i]); //First element of current row

                for (int j = 0; j < GameConfigs.BOARD_ROW_COUNT; j++)
                {
                    int boardIndex = fullCols[i] + j * GameConfigs.BOARD_COL_COUNT;
                    if (board.ValueAt(boardIndex) < colMinValue)
                    {
                        colMinValue = board.ValueAt(boardIndex);
                    }
                }

                clearLinesCount += colMinValue;

                for (int j = 0; j < GameConfigs.BOARD_ROW_COUNT; j++)
                {
                    int boardIndex = fullCols[i] + j * GameConfigs.BOARD_COL_COUNT;
                    board.SetMaskValueAt(colMinValue, boardIndex);
                }
            }
        }

        private void CleanFullRows()
        {
            for (int i = 0; i < fullRows.Count; i++)
            {
                int rowMinValue = board.ValueAt(fullRows[i] * GameConfigs.BOARD_COL_COUNT);

                for (int j = 0; j < GameConfigs.BOARD_COL_COUNT; j++)
                {
                    int boardIndex = fullRows[i] * GameConfigs.BOARD_COL_COUNT + j;
                    if (board.ValueAt(boardIndex) < rowMinValue)
                    {
                        rowMinValue = board.ValueAt(boardIndex);
                    }
                }

                clearLinesCount += rowMinValue;

                for (int j = 0; j < GameConfigs.BOARD_COL_COUNT; j++)
                {
                    int boardIndex = fullRows[i] * GameConfigs.BOARD_COL_COUNT + j;
                    board.SetMaskValueAt(rowMinValue, boardIndex);
                }
            }
        }

        private void CheckFullCols()
        {
            for (int i = 0; i < GameConfigs.BOARD_COL_COUNT; i++)
            {
                int fullColChecker = 1;
                for (int j = 0; j < GameConfigs.BOARD_ROW_COUNT; j++)
                {
                    fullColChecker *= board.ValueAt(j * GameConfigs.BOARD_COL_COUNT + i);
                }

                if (fullColChecker != 0)
                {
                    fullCols.Add(i);
                }
            }
        }

        private void CheckFullRows()
        {
            for (int i = 0; i < GameConfigs.BOARD_ROW_COUNT; i++)
            {
                int fullRowChecker = 1;
                for (int j = 0; j < GameConfigs.BOARD_COL_COUNT; j++)
                {
                    fullRowChecker *= board.ValueAt(i * GameConfigs.BOARD_COL_COUNT + j);
                }

                if (fullRowChecker != 0)
                {
                    fullRows.Add(i);
                }
            }
        }
    }
}