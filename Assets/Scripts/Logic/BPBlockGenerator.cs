﻿using System.Collections;
using System.Collections.Generic;
using Configs;
using MessagePack;
using UnityEngine;
using Utils;

namespace BPLogic
{
    [MessagePackObject]
    public class BPBlockGenerator
    {
        [IgnoreMember] private List<BPBlockTemplateConfig> configs;
        [IgnoreMember] private List<IBPBlockTemplate> templates;

        [IgnoreMember] private List<IBPBlockTemplate> previouslyGeneratedBlocks;

        [IgnoreMember] private int currentBlockIndex = 0;

        public BPBlockGenerator()
        {
        }

        public BPBlockGenerator(List<BPBlockTemplateConfig> configs, List<IBPBlockTemplate> templates,
            List<IBPBlockTemplate> previouslyGeneratedBlocks, int currentBlockIndex)
        {
            this.configs = configs;
            this.templates = templates;
            this.previouslyGeneratedBlocks = previouslyGeneratedBlocks;
            this.currentBlockIndex = currentBlockIndex;
        }

        public void SetBlockConfig(BlockConfig blockConfig)
        {
            if (configs == null)
            {
                configs = new List<BPBlockTemplateConfig>();
            }

            configs.Clear();

            configs.Add(new BPBlockTemplateConfig(new BlockTemplate1x1(), blockConfig.block1x1Rate));
            configs.Add(new BPBlockTemplateConfig(new BlockTemplate1x2(), blockConfig.block1x2Rate));
            configs.Add(new BPBlockTemplateConfig(new BlockTemplate1x3(), blockConfig.block1x3Rate));
            configs.Add(new BPBlockTemplateConfig(new BlockTemplate1x4(), blockConfig.block1x4Rate));
            configs.Add(new BPBlockTemplateConfig(new BlockTemplate2x2L(), blockConfig.block2x2LRate));
            configs.Add(new BPBlockTemplateConfig(new BlockTemplate2x3L(), blockConfig.block2x3LRate));
            configs.Add(new BPBlockTemplateConfig(new BlockTemplate2x3L_2(), blockConfig.block3x2LRate));
            configs.Add(new BPBlockTemplateConfig(new BlockTemplate2x3T(), blockConfig.block2x3TRate));
            configs.Add(new BPBlockTemplateConfig(new BlockTemplate2x2(), blockConfig.block2x2Rate));
            configs.Add(new BPBlockTemplateConfig(new BlockTemplate2x3Z(), blockConfig.block2x3ZRate));
            configs.Add(new BPBlockTemplateConfig(new BlockTemplate2x3Z_R(), blockConfig.block2x3ZRRate));
        }

        public IBPBlockTemplate GetRandomTemplate()
        {
            if (currentBlockIndex == 0)
            {
                CreateTemplatePool();
            }

            IBPBlockTemplate res = templates[Random.Range(0, 1000) % templates.Count];
            previouslyGeneratedBlocks.Add(res);

            CheckAndRemoveDuplicateSamples();

            currentBlockIndex++;
            currentBlockIndex %= GameConfigs.NUMBER_OF_BLOCK;

            return res;
        }

        private void CheckAndRemoveDuplicateSamples()
        {
            bool zShapeExists = previouslyGeneratedBlocks
                .FindIndex(x => x.Type == BlockType.ZShape2x3) >= 0;

            if (zShapeExists)
            {
                RemoveSamplesOfTypes(BlockType.ZShape2x3);
                RemoveSamplesOfTypes(BlockType.ZShape2x3_R);
            }

            bool tShapeExists = previouslyGeneratedBlocks
                .FindIndex(x => x.Type == BlockType.TShape2x3) >= 0;

            if (tShapeExists)
            {
                RemoveSamplesOfTypes(BlockType.TShape2x3);
            }

            int l3count = previouslyGeneratedBlocks
                .FindAll(x => x.Type == BlockType.LShape2x3 || x.Type == BlockType.LShape2x3_2).Count;

            if (l3count >= 2)
            {
                RemoveSamplesOfTypes(BlockType.LShape2x3);
                RemoveSamplesOfTypes(BlockType.LShape2x3_2);
            }

            int square2x2Count = previouslyGeneratedBlocks
                .FindAll(x => x.Type == BlockType.Square2x2).Count;
            if (square2x2Count >= 2)
            {
                RemoveSamplesOfTypes(BlockType.Square2x2);
            }

            int line4Count = previouslyGeneratedBlocks
                .FindAll(x => x.Type == BlockType.Line1x4).Count;
            if (line4Count >= 2)
            {
                RemoveSamplesOfTypes(BlockType.Line1x4);
            }
        }

        private void RemoveSamplesOfTypes(BlockType type)
        {
            List<IBPBlockTemplate> tobeRemoved = new List<IBPBlockTemplate>();

            for (int i = 0; i < templates.Count; i++)
            {
                if (templates[i].Type == type)
                {
                    tobeRemoved.Add(templates[i]);
                }
            }

            for (int i = 0; i < tobeRemoved.Count; i++)
            {
                templates.Remove(tobeRemoved[i]);
            }
        }

        private void RemoveSamples(IBPBlockTemplate sample)
        {
            List<IBPBlockTemplate> tobeRemoved = new List<IBPBlockTemplate>();

            for (int i = 0; i < templates.Count; i++)
            {
                if (templates[i].GetType() == sample.GetType())
                {
                    tobeRemoved.Add(templates[i]);
                }
            }

            for (int i = 0; i < tobeRemoved.Count; i++)
            {
                templates.Remove(tobeRemoved[i]);
            }
        }

        private void OnTemplateTypeReachedCap()
        {
            List<IBPBlockTemplate> tobeRemoved = new List<IBPBlockTemplate>();

            for (int i = 0; i < templates.Count; i++)
            {
                if (templates[i].TileCount == GameConfigs.TEMPLATE_TYPE_TOBE_CLEAN)
                {
                    tobeRemoved.Add(templates[i]);
                }
            }

            for (int i = 0; i < tobeRemoved.Count; i++)
            {
                templates.Remove(tobeRemoved[i]);
            }
        }

        private void CreateTemplatePool()
        {
            if (templates == null)
            {
                templates = new List<IBPBlockTemplate>();
            }

            if (previouslyGeneratedBlocks == null)
            {
                previouslyGeneratedBlocks = new List<IBPBlockTemplate>();
            }

            templates.Clear();
            previouslyGeneratedBlocks.Clear();

            for (int i = 0; i < configs.Count; i++)
            {
                for (int j = 0; j < configs[i].weight; j++)
                {
                    templates.Add(configs[i].template);
                }
            }

            templates.Shuffle();
        }
    }

    public class BPBlockTemplateConfig
    {
        public IBPBlockTemplate template;
        public int weight;

        public BPBlockTemplateConfig(IBPBlockTemplate template, int weight)
        {
            this.template = template;
            this.weight = weight;
        }
    }
}