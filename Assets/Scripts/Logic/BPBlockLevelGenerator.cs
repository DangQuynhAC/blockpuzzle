﻿using System;
using System.Collections;
using System.Collections.Generic;
using MessagePack;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BPLogic
{
    [MessagePackObject]
    public class BPBlockLevelGenerator
    {
        [IgnoreMember] private List<BPBlockLevelConfig> levelConfigs;

        public BPBlockLevelGenerator()
        {
        }

        public BPBlockLevelGenerator( List<BPBlockLevelConfig> levelConfigs)
        {
            this.levelConfigs = levelConfigs;
        }

        public void SetLevelConfig(List<BPBlockLevelConfig> levelConfigs)
        {
            this.levelConfigs = levelConfigs;
            // this.levelConfigs = GameConfigs.BLOCK_LEVEL_CONFIGS;
        }

        public void SetLevelsForBlocks(BPBlock[] blocks)
        {
            int totalTiles = GetTotalTilesCount(blocks);

            for (int i = levelConfigs.Count - 1; i >= 0; i--)
            {
                BPBlockLevelConfig currentConfig = levelConfigs[i];
                int maxTilesCountForCurrentLevel = GetMaxTilesCountForALevelByWeight(totalTiles, currentConfig.weight);
                for (int j = 0; j < maxTilesCountForCurrentLevel; j++)
                {
                    SetLevelForBlocks(currentConfig.level, blocks);
                }
            }
        }

        private void SetLevelForBlocks(int level, BPBlock[] blocks)
        {
            if (level > 1)
            {
                int current = Random.Range(0, 1000) % blocks.Length;
                if (!blocks[current].SetLevelRandomly(level))
                {
                    ForceSetLevelForBlocks(level, blocks);
                }
            }
        }

        private void ForceSetLevelForBlocks(int level, BPBlock[] blocks)
        {
            for (int i = 0; i < blocks.Length; i++)
            {
                if (blocks[i].MeaningfulIndexesAvailableForSettingLevel)
                {
                    blocks[i].SetLevelRandomly(level);
                    return;
                }
            }
        }

        private int GetMaxTilesCountForALevelByWeight(int totalTilesCount, float weight)
        {
            return Mathf.RoundToInt(totalTilesCount * weight / 100);
        }

        private int GetTotalTilesCount(BPBlock[] blocks)
        {
            int sum = 0;

            for (int i = 0; i < blocks.Length; i++)
            {
                sum += blocks[i].TilesCount;
            }

            return sum;
        }
    }

    [Serializable]
    public class BPBlockLevelConfig
    {
        public int level;
        public float weight;

        public BPBlockLevelConfig(int level, float weight)
        {
            this.level = level;
            this.weight = weight;
        }
    }
}