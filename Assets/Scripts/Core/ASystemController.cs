﻿using System.Collections;
using System.Collections.Generic;
using BPData.ProfileData;
using UnityEngine;

namespace BPCore
{
    public abstract class ASystemController : MonoBehaviour
    {
        public abstract void Init();
        public abstract void OnProfileDataLoaded(ProfileData profileData);
    }

}