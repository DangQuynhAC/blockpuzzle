﻿using System;
using System.Collections;
using System.Collections.Generic;
using BPData.ProfileData;
using BPLogic;
using UnityEngine;
using BPViews;
using BPPools;

namespace BPCore
{
    public class PoolController : ASystemController
    {
        [SerializeField] private CellView cellViewSample;
        private static CellViewPool cellViewPool;

        [SerializeField] private TileView tileViewSample;
        private static TileViewPool tileViewPool;
        
        public override void Init()
        {
            cellViewPool = new CellViewPool(cellViewSample);
            cellViewPool.CreatePool(GameConfigs.BOARD_ROW_COUNT * GameConfigs.BOARD_COL_COUNT);
            
            tileViewPool = new TileViewPool(tileViewSample);
            tileViewPool.CreatePool(100);
        }

        public override void OnProfileDataLoaded(ProfileData profileData)
        {
            
        }

        public static CellView GetCellView()
        {
            return cellViewPool.Get();
        }

        public static TileView GetTileView()
        {
            return tileViewPool.Get();
        }

        public static void Destroy(TileView tileView)
        {
            tileViewPool.Destroy(tileView);
        }
    }
}