﻿using System.Collections;
using System.Collections.Generic;
using BPCore;
using BPData.ProfileData;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BPMeta
{
    public class TrophyController : ASystemController
    {
        [SerializeField] private Button collectTrophiesButton;

        [SerializeField] private TextMeshProUGUI currentTrophies;

        [SerializeField] private TextMeshProUGUI targetTrophies;

        [SerializeField] private TextMeshProUGUI totalTrophies;

        private TrophyData trophyData;

        public TrophyData Data => trophyData;

        public override void Init()
        {
            collectTrophiesButton.onClick.AddListener(HandleCollectTrophies);
        }

        public override void OnProfileDataLoaded(ProfileData profileData)
        {
            trophyData = profileData.trophyData;
            OnTrophiesDataUpdate();
        }

        public void OnResetGame()
        {
            trophyData.ResetDataToStartValue();
            OnTrophiesDataUpdate();
        }

        private void HandleCollectTrophies()
        {
            trophyData.AddTrophies();
            RaiseTrophiesTarget();
            OnTrophiesDataUpdate();
        }

        private void RaiseTrophiesTarget()
        {
            trophyData.UpdateTrophiesTarget();
        }

        private void OnTrophiesDataUpdate()
        {
            currentTrophies.text = $"Current Trophies: {trophyData.currentTrophies}";
            targetTrophies.text = $"Will Gain: {trophyData.targetTrophies}";
            totalTrophies.text = $"Total Trophies: {trophyData.totalTrophies}";
        }
    }
}