﻿using System.Collections;
using System.Collections.Generic;
using BPCore;
using BPData.ProfileData;
using BPLogic;
using TMPro;
using UnityEngine;

namespace Core
{
    public class ScoreController : ASystemController
    {
        [SerializeField] private TextMeshProUGUI bestScoreText;
        [SerializeField] private TextMeshProUGUI currentScoreText;

        private ScoreData scoreData;

        public ScoreData Data => scoreData;

        public int CurrentScore => scoreData.CurrentScore;

        public override void Init()
        {
            scoreData = new ScoreData(0, 0);
            UpdateScoreView();
        }

        public override void OnProfileDataLoaded(ProfileData profileData)
        {
            scoreData = profileData.scoreData;
            UpdateScoreView();
        }

        public void HandleRemoveLines(int linesCount)
        {
            float scoreMultiplier = GetComboMultiplier(linesCount);
            int score = Mathf.RoundToInt(scoreMultiplier * ScoreConfigs.BASE_POINT * linesCount);
            AddScore(score);
        }

        public void HandlePutBlock(int tilesCount)
        {
            AddScore(tilesCount);
        }

        public void OnResetGame()
        {
            scoreData.ResetScore();
            OnScoreUpdate();
        }

        private void AddScore(int amount)
        {
            scoreData.AddScore(amount);
            OnScoreUpdate();
        }

        private void OnScoreUpdate()
        {
            CheckBreakBestScore();
            UpdateScoreView();
        }

        private void CheckBreakBestScore()
        {
            scoreData.CheckBreakHighScore();
        }

        private void UpdateScoreView()
        {
            bestScoreText.text = $"Best: {scoreData.BestScore}";
            currentScoreText.text = $"Current: {scoreData.CurrentScore}";
        }

        private float GetComboMultiplier(int removedLinesCount)
        {
            switch (removedLinesCount)
            {
                case 1:
                    return 1;
                case 2:
                    return ScoreConfigs.COMBO_MULTIPLIER_2;
                case 3:
                    return ScoreConfigs.COMBO_MULTIPLIER_3;
                case 4:
                    return ScoreConfigs.COMBO_MULTIPLIER_4;
                case 5:
                    return ScoreConfigs.COMBO_MULTIPLIER_5;
                default:
                    return 0;
            }
        }
    }
}