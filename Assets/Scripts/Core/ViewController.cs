﻿using System.Collections;
using System.Collections.Generic;
using BPData.ProfileData;
using BPViews;
using UnityEngine;

namespace BPCore
{
    public class ViewController : ASystemController
    {
        [SerializeField] private BoardView boardView;

        [SerializeField] private List<BlockView> blockViews;
        
        public override void Init()
        {
            boardView.Init();
            for (int i = 0; i < blockViews.Count; i++)
            {
                blockViews[i].Init();
            }
        }

        public override void OnProfileDataLoaded(ProfileData profileData)
        {

        }

        public void OnResetGame()
        {
            boardView.ResetView();
            for (int i = 0; i <  blockViews.Count; i++)
            {
                blockViews[i].ResetView();
            }
        }
    }

}