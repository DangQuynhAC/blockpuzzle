﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using BPData.ProfileData;
using BPLogic;
using MessagePack;
using MessagePack.Resolvers;
using UnityEngine;

namespace BPCore
{
    public class DataController : ASystemController
    {
        private const string dataFileName = "gameData.data";
        private string filePath;

        public override void Init()
        {
            filePath = Path.Combine(Application.persistentDataPath, dataFileName);
        }

        public override void OnProfileDataLoaded(ProfileData profileData)
        {
        }

        public void SaveGame(BPGame game, ScoreData scoreData, CollectibleData collectibleData, BoosterData boosterData,
            TrophyData trophyData)
        {
            var resolver = MessagePack.Resolvers.CompositeResolver.Create(
                GeneratedResolver.Instance,
                MessagePack.Resolvers.BuiltinResolver.Instance
            );
            var options = MessagePackSerializerOptions.Standard.WithResolver(resolver);

            var saveData = new ProfileData(game, scoreData, collectibleData, boosterData, trophyData);
            // var saveData = new ProfileData(game, scoreData, collectibleData, boosterData);
            var data = MessagePackSerializer.Serialize(saveData, options);
            File.WriteAllBytes(filePath, data);
        }

        public bool Load(out ProfileData profileData)
        {
            if (File.Exists(filePath))
            {
                var resolver = MessagePack.Resolvers.CompositeResolver.Create(
                    GeneratedResolver.Instance,
                    MessagePack.Resolvers.BuiltinResolver.Instance
                );
                var options = MessagePackSerializerOptions.Standard.WithResolver(resolver);

                var data = File.ReadAllBytes(filePath);
                profileData = MessagePackSerializer.Deserialize<ProfileData>(data,
                    options);
                return true;
            }

            profileData = new ProfileData(
                new BPGame(),
                new ScoreData(currentScore: 0, bestScore: 0),
                new CollectibleData(collectibleTarget: GameConfigs.COLLECTIBLES_REQUIRED_START, collectibleCurrent: 0,
                    collectibleTotal: 0),
                new BoosterData(bombCount: 0),
                new TrophyData(GameConfigs.TROPHIES_GAINED_START, 0, 0)
            );

            // profileData = new ProfileData(
            //     new BPGame(),
            //     new ScoreData(currentScore: 0, bestScore: 0),
            //     new CollectibleData(collectibleTarget: 20, collectibleCurrent: 0, collectibleTotal: 0),
            //     new BoosterData(bombCount: 0)
            // );

            return false;
        }
    }
}