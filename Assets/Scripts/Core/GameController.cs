﻿using System;
using System.Collections;
using System.Collections.Generic;
using BPController;
using BPData.ProfileData;
using Controllers;
using UnityEngine;

namespace BPCore
{
    public class GameController : ASystemController
    {
        public event Action<int> onRotatingBlock = i => { };  
        public event Action<int, List<int>> onDroppingBlockIntoBoard = (i, ints) => { };
        public BoardController.EmptyTileChecker emptyTileChecker = i => false;

        [SerializeField] private BoardController boardController;
        [SerializeField] private List<BlockController> blockControllers;

        private int activeControllerIndex = -1;

        public override void Init()
        {
            boardController.Init();
            boardController.onDroppingBlockIntoBoard += HandleDroppingBlockIntoBoard;
            boardController.emptyTileChecker += HandleEmptyTileCheck;
            
            for (int i = 0; i < blockControllers.Count; i++)
            {
                blockControllers[i].Init();
                blockControllers[i].SetIndex(i);
                blockControllers[i].onBlockMoving += boardController.HandleBlockMoving;
                blockControllers[i].onBlockStopping += boardController.HandleBlockStopping;
                blockControllers[i].onBlockBeginMoving += HandleBlockControllerBeginMoving;
                blockControllers[i].onBlockRotating += HandleBlockRotating;
            }
        }

        public override void OnProfileDataLoaded(ProfileData profileData)
        {
            
        }

        public void OnResetGame()
        {
            boardController.ResetController();
            for (int i = 0; i < blockControllers.Count; i++)
            {
                blockControllers[i].ResetController();
            }
        }

        public void HandleGameGenerateNewBlock(int index, int[] data)
        {
            blockControllers[index].SetData(data);
        }

        public void HandleRemoveTiles(List<int> indexesToRemove)
        {
            boardController.HandleRemoveTiles(indexesToRemove);
        }

        public void HandleBoardDataUpdate(int[] board)
        {
            boardController.HandleUpdateView(board);
        }

        private void HandleBlockControllerBeginMoving(int index)
        {
            activeControllerIndex = index;
        }

        private void HandleDroppingBlockIntoBoard(List<int> validIndexes)
        {
            onDroppingBlockIntoBoard(activeControllerIndex, validIndexes);
        }

        private void HandleBlockRotating(int blockIndex)
        {
            onRotatingBlock(blockIndex);
        }

        private bool HandleEmptyTileCheck(int index)
        {
            return emptyTileChecker(index);
        }
    }
}