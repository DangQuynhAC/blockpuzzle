﻿using System;
using System.Collections;
using System.Collections.Generic;
using BPData.ProfileData;
using BPLogic;
using BPMeta;
using Configs;
using Core;
using Meta.Boosters;
using UnityEngine;
using UnityEngine.UI;

namespace BPCore
{
    public class BlockPuzzleManager : MonoBehaviour
    {
        private PoolController poolController;
        private ViewController viewController;
        private GameController gameController;
        private ScoreController scoreController;
        private CollectibleController collectibleController;
        private BoostersController boostersController;
        private DataController dataController;
        private PopUpController popUpController;
        private TrophyController trophyController;

        private ProfileData profileData;

        private BPGame bpGame;

        private BlockGenerateConfig blockGenerateConfig;

        private bool saveSuccess;

        [SerializeField] private GameObject gameOverPanel;
        [SerializeField] private Button playAgainButton;

        private void Awake()
        {
            Application.targetFrameRate = 60;

            poolController = GetComponentInChildren<PoolController>();
            poolController.Init();

            viewController = GetComponentInChildren<ViewController>();
            viewController.Init();

            gameController = GetComponentInChildren<GameController>();
            gameController.Init();
            gameController.onDroppingBlockIntoBoard += HandleDroppingBlockIntoBoard;
            gameController.onRotatingBlock += HandleRotatingBlock;
            gameController.emptyTileChecker += HandleEmptyTileCheck;

            scoreController = GetComponentInChildren<ScoreController>();
            scoreController.Init();

            collectibleController = GetComponentInChildren<CollectibleController>();
            collectibleController.Init();
            collectibleController.onTargetReached += HandleCollectiblesTargetReached;

            playAgainButton.onClick.AddListener(HandlePlayAgainButtonPressed);

            blockGenerateConfig = Resources.Load<BlockGenerateConfig>("BlockConfig");

            boostersController = GetComponentInChildren<BoostersController>();
            boostersController.Init();

            dataController = GetComponentInChildren<DataController>();
            dataController.Init();

            popUpController = GetComponentInChildren<PopUpController>();
            popUpController.Init();

            trophyController = GetComponentInChildren<TrophyController>();
            trophyController.Init();

            StartGame();
        }

        private void OnApplicationQuit()
        {
            if (!saveSuccess)
            {
                SaveProfileData();
            }
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus)
            {
                if (!saveSuccess)
                {
                    SaveProfileData();
                    saveSuccess = true;
                }
            }
            else
            {
                saveSuccess = false;
            }
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                if (!saveSuccess)
                {
                    SaveProfileData();
                    saveSuccess = true;
                }
            }
            else
            {
                saveSuccess = false;
            }
        }

        private void SaveProfileData()
        {
            dataController.SaveGame(
                bpGame,
                scoreController.Data,
                collectibleController.Data,
                boostersController.Data,
                trophyController.Data
            );
        }

        private bool LoadGame()
        {
            bool loadSucess = dataController.Load(out profileData);

            bpGame = profileData.game;
            bpGame.onGenerateNewBlock += gameController.HandleGameGenerateNewBlock;
            bpGame.onBoardUpdate += gameController.HandleBoardDataUpdate;
            bpGame.onGameOver += HandleGameOver;
            bpGame.onClearLines += scoreController.HandleRemoveLines;
            bpGame.onPutBlock += scoreController.HandlePutBlock;
            bpGame.onPrepareToGenerateNewBlock += SetConfigByScore;
            bpGame.onCollectTiles += collectibleController.Collect;

            boostersController.OnProfileDataLoaded(profileData);
            scoreController.OnProfileDataLoaded(profileData);
            collectibleController.OnProfileDataLoaded(profileData);
            trophyController.OnProfileDataLoaded(profileData);

            return loadSucess;
        }

        public void StartGame()
        {
            if (!LoadGame())
            {
                bpGame.Start();
            }
            else
            {
                bpGame.StartFromSaveData();
            }
        }

        private void SetConfigByScore()
        {
            if (scoreController.CurrentScore >=
                blockGenerateConfig.Configs[blockGenerateConfig.Configs.Count - 1].targetScore)
            {
                bpGame.SetScoreBasedConfig(blockGenerateConfig.Configs[blockGenerateConfig.Configs.Count - 1]);
            }
            else
            {
                for (int i = 0; i < blockGenerateConfig.Configs.Count; i++)
                {
                    if (scoreController.CurrentScore < blockGenerateConfig.Configs[i].targetScore)
                    {
                        bpGame.SetScoreBasedConfig(blockGenerateConfig.Configs[i - 1]);
                        return;
                    }
                }
            }
        }

        private void HandleDroppingBlockIntoBoard(int controllerIndex, List<int> validIndexes)
        {
            bpGame.PutBlock(controllerIndex, validIndexes);
        }

        private void HandleRotatingBlock(int blockIndex)
        {
            bpGame.RotateBlock(blockIndex);
        }

        private bool HandleEmptyTileCheck(int index)
        {
            return bpGame.IsEmptyAt(index);
        }

        private void HandleGameOver()
        {
            gameOverPanel.SetActive(true);
        }

        private void HandlePlayAgainButtonPressed()
        {
            gameOverPanel.SetActive(false);
            gameController.OnResetGame();
            viewController.OnResetGame();
            scoreController.OnResetGame();
            collectibleController.OnResetGame();
            trophyController.OnResetGame();

            bpGame.Reset();
        }

        private void HandleCollectiblesTargetReached()
        {
            // collectibleController.Consume();
            // collectibleController.UpdateCollectiblesTarget();
        }
    }
}