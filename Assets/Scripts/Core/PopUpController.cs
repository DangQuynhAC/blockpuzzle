﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using BPData.ProfileData;
using BPUI.PopUps;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace BPCore
{
    public class PopUpController : ASystemController
    {
        [SerializeField] private List<BasePopUp> popups;

        [SerializeField] private Image blackImage;

        private Dictionary<EPopUpID, BasePopUp> popupsMap;

        private List<BasePopUp> currentlyShowingPopUps;

        private BasePopUp currentPopUp;

        private List<PopUpData> popupsQueue;

        private bool readyToProceedNextPopup = true;

        private bool processShowingPopUpRunning;

        private bool processHidingPopUpsRunning;

        public override void Init()
        {
            BuildPopupsMap();
            popupsQueue = new List<PopUpData>();
            currentlyShowingPopUps = new List<BasePopUp>();
        }

        public override void OnProfileDataLoaded(ProfileData profileData)
        {
        }

        public void Show(EPopUpID popUpID, params object[] args)
        {
            if (!PopUpAlreadyQueued(popUpID))
            {
                popupsQueue.Add(new PopUpData(popUpID, args));
            }

            if (!processShowingPopUpRunning)
            {
                Timing.RunCoroutine(ProcessShowingAllQueuedPopups());
            }
        }

        public void Hide(EPopUpID popUpID, params object[] args)
        {
            Timing.RunCoroutine(ProceedHidingPopUp(popUpID));
        }

        private IEnumerator<float> ProcessShowingAllQueuedPopups()
        {
            processShowingPopUpRunning = true;

            while (popupsQueue.Count > 0)
            {
                if (!readyToProceedNextPopup)
                {
                    yield return -1;
                }

                var nextPopUpToShow = popupsQueue[0];
                popupsQueue.RemoveAt(0);

                Timing.RunCoroutine(ProceedShowingPopUp(nextPopUpToShow));
            }

            processShowingPopUpRunning = false;

            yield return -1;
        }

        private IEnumerator<float> ProceedShowingPopUp(PopUpData popUpData)
        {
            while (!readyToProceedNextPopup)
            {
                yield return -1;
            }

            currentPopUp = popupsMap[popUpData.popUpID];
            currentPopUp.onShown += OnPopUpShown;
            currentPopUp.onHidden += OnPopUpHidden;

            ShowBlackImage(currentPopUp);

            if (!currentPopUp.allowMultiplePopUps && currentlyShowingPopUps.Count > 0)
            {
                processHidingPopUpsRunning = true;
                Timing.RunCoroutine(ProceedHidingAllPopUps());

                while (processHidingPopUpsRunning)
                {
                    yield return -1;
                }

                readyToProceedNextPopup = false;
                currentPopUp.Show(popUpData.data);
                currentlyShowingPopUps.Add(currentPopUp);
            }
            else
            {
                readyToProceedNextPopup = false;
                currentPopUp.Show(popUpData.data);
                currentlyShowingPopUps.Add(currentPopUp);
            }

            yield return -1;
        }

        private IEnumerator<float> ProceedHidingPopUp(EPopUpID popUpID)
        {
            while (!readyToProceedNextPopup)
            {
                yield return -1;
            }

            int currentPopUpIndex = PopUpIsShowing(popUpID);

            if (currentPopUpIndex == -1)
            {
                readyToProceedNextPopup = true;
                yield return -1;
            }
            else
            {
                currentlyShowingPopUps[currentPopUpIndex].Hide();
                currentlyShowingPopUps.RemoveAt(currentPopUpIndex);
            }
        }

        private IEnumerator<float> ProceedHidingAllPopUps()
        {
            while (readyToProceedNextPopup && currentlyShowingPopUps.Count != 0)
            {
                Timing.RunCoroutine(
                    ProceedHidingPopUp(currentlyShowingPopUps[currentlyShowingPopUps.Count - 1].PopUpID));
            }

            processHidingPopUpsRunning = false;
            yield return -1;
        }

        private void OnPopUpShown()
        {
            readyToProceedNextPopup = true;
        }

        private void OnPopUpHidden()
        {
            ShowPreviousPopUp();
            readyToProceedNextPopup = true;
        }

        private int PopUpIsShowing(EPopUpID popUpID)
        {
            for (int i = 0; i < currentlyShowingPopUps.Count; i++)
            {
                if (currentlyShowingPopUps[i].PopUpID == popUpID)
                {
                    return i;
                }
            }

            return -1;
        }

        private bool PopUpAlreadyQueued(EPopUpID popUpID)
        {
            for (int i = 0; i < popupsQueue.Count; i++)
            {
                if (popupsQueue[i].popUpID == popUpID)
                {
                    return true;
                }
            }

            return false;
        }

        private void BuildPopupsMap()
        {
            popupsMap = new Dictionary<EPopUpID, BasePopUp>();
            for (int i = 0; i < popups.Count; i++)
            {
                popupsMap[popups[i].PopUpID] = popups[i];
            }
        }

        private void ShowPreviousPopUp()
        {
            if (currentlyShowingPopUps.Count > 0)
            {
                currentPopUp = currentlyShowingPopUps[currentlyShowingPopUps.Count - 1];
                ShowBlackImage(currentPopUp);
            }
            else
            {
                EnableBlackImage(false);
            }
        }

        private void ShowBlackImage(BasePopUp popUp)
        {
            if (popUp.shouldShowBlackImage)
            {
                blackImage.transform.SetAsLastSibling();
                popUp.transform.SetAsLastSibling();
            }

            EnableBlackImage(popUp.shouldShowBlackImage);
        }

        private void EnableBlackImage(bool enable)
        {
            blackImage.enabled = enable;
        }
    }
}