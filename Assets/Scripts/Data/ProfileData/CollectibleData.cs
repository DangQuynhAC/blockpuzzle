﻿using System.Collections;
using System.Collections.Generic;
using BPLogic;
using MessagePack;
using UnityEngine;

namespace BPData.ProfileData
{
    [MessagePackObject]
    public struct CollectibleData
    {
        [Key(0)] public int collectibleTarget;

        [Key(1)] public int collectibleCurrent;

        [Key(2)] public int collectibleTotal;

        [IgnoreMember] public int CollectibleTotal => collectibleTotal;

        public CollectibleData(int collectibleTarget, int collectibleCurrent, int collectibleTotal)
        {
            this.collectibleTarget = collectibleTarget;
            this.collectibleCurrent = collectibleCurrent;
            this.collectibleTotal = collectibleTotal;
        }

        public void UpdateCollectibleTarget()
        {
            collectibleTarget = Mathf.RoundToInt(collectibleTarget * GameConfigs.COLLECTIBLES_REQUIRED_MULTIPLIER);
        }

        public void ResetDataToStartValue()
        {
            collectibleCurrent = 0;
            collectibleTarget = GameConfigs.COLLECTIBLES_REQUIRED_START;
        }

        public void AddCollectibles(int amount)
        {
            collectibleCurrent += amount;
            collectibleTotal += amount;
        }

        public void OnClaimReward()
        {
            collectibleCurrent -= collectibleTarget;
        }

        public float CurrentProgress()
        {
            return (float) collectibleCurrent / collectibleTarget;
        }

        public bool DidTargetReach()
        {
            return collectibleCurrent >= collectibleTarget;
        }

        public void Spend(int amount)
        {
            if (collectibleTotal >= amount)
            {
                collectibleTotal -= amount;
            }
        }

        public bool IsEnoughCollectiblesForMakingPurchase(int requirement)
        {
            return collectibleTotal >= requirement;
        }
    }
}