﻿using System.Collections;
using System.Collections.Generic;
using MessagePack;
using UnityEngine;

namespace BPData.ProfileData
{
    [MessagePackObject]
    public struct ScoreData
    {
        [Key(0)] public int currentScore;

        [Key(1)] public int bestScore;

        [IgnoreMember] public int BestScore => bestScore;

        [IgnoreMember] public int CurrentScore => currentScore;

        public ScoreData(int currentScore, int bestScore)
        {
            this.currentScore = currentScore;
            this.bestScore = bestScore;
        }

        public void AddScore(int score)
        {
            currentScore += score;
        }

        public void ResetScore()
        {
            currentScore = 0;
        }

        public void CheckBreakHighScore()
        {
            if (currentScore > bestScore)
            {
                bestScore = currentScore;
            }
        }
    }
}