﻿using System.Collections;
using System.Collections.Generic;
using MessagePack;
using UnityEngine;

namespace BPData.ProfileData
{
    [MessagePackObject]
    public struct BoosterData
    {
        [Key(0)] public int bombCount;

        public BoosterData(int bombCount)
        {
            this.bombCount = bombCount;
        }

        public void ConsumeBomb()
        {
            bombCount -= 1;
            bombCount = bombCount < 0 ? 0 : bombCount;
        }

        public void AddBombs(int amount)
        {
            bombCount += amount;
        }
    }
}