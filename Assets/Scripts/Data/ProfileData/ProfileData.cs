﻿using System.Collections;
using System.Collections.Generic;
using BPLogic;
using MessagePack;
using UnityEngine;

namespace BPData.ProfileData
{
    [MessagePackObject]
    public struct ProfileData
    {
        [Key(0)] public readonly BPGame game;
        [Key(1)] public readonly ScoreData scoreData;
        [Key(2)] public readonly CollectibleData collectibleData;
        [Key(3)] public readonly BoosterData boosterData;
        [Key(4)] public readonly TrophyData trophyData;

        public ProfileData(BPGame game, ScoreData scoreData, CollectibleData collectibleData,
            BoosterData boosterData, TrophyData trophyData)
        {
            this.game = game;
            this.scoreData = scoreData;
            this.collectibleData = collectibleData;
            this.boosterData = boosterData;
            this.trophyData = trophyData;
        }
    }
}