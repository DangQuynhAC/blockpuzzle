﻿using System.Collections;
using System.Collections.Generic;
using BPLogic;
using MessagePack;
using UnityEngine;

namespace BPData.ProfileData
{
    [MessagePackObject]
    public struct TrophyData
    {
        [Key(0)] public int targetTrophies;
        [Key(1)] public int currentTrophies;
        [Key(2)] public int totalTrophies;

        public TrophyData(int targetTrophies, int currentTrophies, int totalTrophies)
        {
            this.targetTrophies = targetTrophies;
            this.currentTrophies = currentTrophies;
            this.totalTrophies = totalTrophies;
        }

        public void ResetDataToStartValue()
        {
            targetTrophies = GameConfigs.TROPHIES_GAINED_START;
        }

        public void AddTrophies()
        {
            totalTrophies += targetTrophies;
            currentTrophies += targetTrophies;
        }

        public void SpendTrophies(int amount)
        {
            currentTrophies -= amount;
        }

        public void UpdateTrophiesTarget()
        {
            targetTrophies = Mathf.RoundToInt(targetTrophies * GameConfigs.TROPHIES_GAINED_MULTIPLIER);
        }
    }
}