﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BPPools
{
    public interface IPool<T> where T : IPoolable
    {
        void CreatePool(int poolSize);
        T Get();
        void Destroy(T anObject);
    }
}