﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BPPools
{
    public interface IPoolable
    {
        void Active();
        void Dispose();
    }
}