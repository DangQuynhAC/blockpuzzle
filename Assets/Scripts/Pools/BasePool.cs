﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BPPools
{
    public abstract class BasePool<T> : IPool<T> where T : Object, IPoolable 
    {
        private T sample;

        private List<T> poolables;
        
        public BasePool(T sample)
        {
            this.sample = sample;
        }

        public virtual void CreatePool(int poolSize)
        {
            poolables = new List<T>(poolSize);
            
            for (int i = 0; i < poolSize; i++)
            {
                T o = Object.Instantiate(sample);
                o.Dispose();
                poolables.Add(o);
            }
        }

        public virtual T Get()
        {
            T o = poolables[0];
            poolables.RemoveAt(0);
            o.Active();
            return o;
        }

        public virtual void Destroy(T anObject)
        {
            anObject.Dispose();
            poolables.Add(anObject);
        }
    }
}