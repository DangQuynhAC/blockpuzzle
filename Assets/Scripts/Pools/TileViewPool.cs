﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BPViews;

namespace BPPools
{
    public class TileViewPool : BasePool<TileView>
    {
        public TileViewPool(TileView sample) : base(sample)
        {
        }
    }

}