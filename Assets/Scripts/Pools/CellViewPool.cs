﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BPViews;

namespace BPPools
{
    public class CellViewPool : BasePool<CellView>
    {
        public CellViewPool(CellView sample) : base(sample)
        {
        }
    }
}