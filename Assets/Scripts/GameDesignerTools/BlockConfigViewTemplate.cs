﻿using System.Collections;
using System.Collections.Generic;
using BPLogic;
using TMPro;
using UnityEngine;

namespace GameDesignerTools
{
    public class BlockConfigViewTemplate : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI blockLabel;
        [SerializeField] private TMP_InputField weight;

        public void SetBlock(int[] block)
        {
            string res = "";
            for (int i = 0; i < GameConfigs.BLOCK_COL_COUNT; i++)
            {
                for (int j = 0; j < GameConfigs.BLOCK_COL_COUNT; j++)
                {
                    int blockIndex = i * GameConfigs.BLOCK_COL_COUNT + j;
                    res = string.Format("{0}{1}", res, block[blockIndex] == 0 ? " " : "*");
                }

                res += "\n";
            }

            blockLabel.text = res;
        }

        public void SetWeight(int weight)
        {
            this.weight.text = weight.ToString();
        }

        public int GetWeight()
        {
            return int.Parse(weight.text);
        }
    }
}