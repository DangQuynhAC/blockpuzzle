﻿using System.Collections;
using System.Collections.Generic;
using BPLogic;
using UnityEngine;

namespace GameDesignerTools
{
    public class LevelConfigController : MonoBehaviour
    {
        [SerializeField] private LevelConfigTemplate template;

        private List<LevelConfigTemplate> configs = new List<LevelConfigTemplate>();
        
        public void SetUpView(int max)
        {
            configs.Clear();
            
            for (int i = 2; i <= max; i++)
            {
                var config = Instantiate(template, transform);
                config.gameObject.SetActive(true);
                config.SetLevel(i);

                configs.Add(config);
            }
        }


        public void ParseLevelConfigs()
        {
            List<BPBlockLevelConfig> res = new List<BPBlockLevelConfig>();
            
            for (int i = 0; i < configs.Count; i++)
            {
                var config = new BPBlockLevelConfig(configs[i].level, configs[i].GetWeight());
                res.Add(config);
            }

            GameConfigs.BLOCK_LEVEL_CONFIGS = res;
        }
    }

}