﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace GameDesignerTools
{
    public class LevelConfigTemplate : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI levelLabel;
        [SerializeField] private TMP_InputField weightInput;

        [HideInInspector] public int level;

        public void SetLevel(int level)
        {
            this.level = level;
            levelLabel.text = this.level.ToString();
        }

        public float GetWeight()
        {
            return float.Parse(weightInput.text);
        }
    }
}