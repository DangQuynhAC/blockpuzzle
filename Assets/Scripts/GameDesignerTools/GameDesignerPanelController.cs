﻿using System;
using System.Collections;
using System.Collections.Generic;
using BPCore;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameDesignerTools
{
    public class GameDesignerPanelController : MonoBehaviour
    {
        [SerializeField] private TMP_InputField maxLevelInput;
        [SerializeField] private Button maxLevelConfirmButton;
        [SerializeField] private Button startButton;

        [SerializeField] private LevelConfigController levelConfigController;
        [SerializeField] private BlockPuzzleManager blockPuzzleManager;

        [SerializeField] private BlockTemplateConfigController blockTemplateConfigController;

        private void Start()
        {
            maxLevelConfirmButton.onClick.AddListener(SetUpView);
            startButton.onClick.AddListener(StartGame);
        }

        private void StartGame()
        {
            ParseLevelConfig();
            
            blockPuzzleManager.StartGame();
            gameObject.SetActive(false);
        }

        private void ParseLevelConfig()
        {
            levelConfigController.ParseLevelConfigs();
            blockTemplateConfigController.ParseConfigData();
        }

        private void SetUpView()
        {
            levelConfigController.SetUpView(int.Parse(maxLevelInput.text));
        }
    }

}