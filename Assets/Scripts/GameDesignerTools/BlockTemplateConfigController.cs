﻿using System;
using System.Collections;
using System.Collections.Generic;
using BPLogic;
using Core;
using UnityEngine;

namespace GameDesignerTools
{
    public class BlockTemplateConfigController : MonoBehaviour
    {
        [SerializeField] private BlockConfigViewTemplate template;
        private List<BlockConfigViewTemplate> views;

        private void Start()
        {
            InitTemplateData();
            SetupView();
        }

        private void InitTemplateData()
        {
            // GameConfigs.BLOCK_TEMPLATE_CONFIGS.Add(new BPBlockTemplateConfig(new BlockTemplate1x1(), 1));
            // GameConfigs.BLOCK_TEMPLATE_CONFIGS.Add(new BPBlockTemplateConfig(new BlockTemplate1x2(), 1));
            // GameConfigs.BLOCK_TEMPLATE_CONFIGS.Add(new BPBlockTemplateConfig(new BlockTemplate1x3(), 1));
            // GameConfigs.BLOCK_TEMPLATE_CONFIGS.Add(new BPBlockTemplateConfig(new BlockTemplate1x4(), 1));
            GameConfigs.BLOCK_TEMPLATE_CONFIGS.Add(new BPBlockTemplateConfig(new BlockTemplate2x2L(), 1));
            // GameConfigs.BLOCK_TEMPLATE_CONFIGS.Add(new BPBlockTemplateConfig(new BlockTemplate2x3L(), 1));
            // GameConfigs.BLOCK_TEMPLATE_CONFIGS.Add(new BPBlockTemplateConfig(new BlockTemplate2x3L_2(), 1));
            // GameConfigs.BLOCK_TEMPLATE_CONFIGS.Add(new BPBlockTemplateConfig(new BlockTemplate2x3T(), 1));
            // GameConfigs.BLOCK_TEMPLATE_CONFIGS.Add(new BPBlockTemplateConfig(new BlockTemplate2x2(), 1));
        }

        private void SetupView()
        {
            views = new List<BlockConfigViewTemplate>();

            for (int i = 0; i < GameConfigs.BLOCK_TEMPLATE_CONFIGS.Count; i++)
            {
                var view = Instantiate(template, transform);
                view.gameObject.SetActive(true);
                view.SetBlock(GameConfigs.BLOCK_TEMPLATE_CONFIGS[i].template.Data);
                view.SetWeight(GameConfigs.BLOCK_TEMPLATE_CONFIGS[i].weight);

                views.Add(view);
            }
        }

        public void ParseConfigData()
        {
            for (int i = 0; i < GameConfigs.BLOCK_TEMPLATE_CONFIGS.Count; i++)
            {
                GameConfigs.BLOCK_TEMPLATE_CONFIGS[i].weight = views[i].GetWeight();
            }
        }
    }
}