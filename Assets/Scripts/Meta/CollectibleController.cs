﻿using System;
using System.Collections;
using System.Collections.Generic;
using BPCore;
using BPData.ProfileData;
using BPUI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BPMeta
{
    public class CollectibleController : ASystemController, ICollectibleCallbackHandler
    {
        public event Action onTargetReached = () => { };

        [SerializeField] private Button consumeCollectiblesButton;

        [SerializeField] private Slider collectibleProgress;

        [SerializeField] private TextMeshProUGUI totalText;

        private CollectibleData collectibleData;

        public CollectibleData Data => collectibleData;

        public override void Init()
        {
            consumeCollectiblesButton.onClick.AddListener(Consume);
        }

        public override void OnProfileDataLoaded(ProfileData profileData)
        {
            collectibleData = profileData.collectibleData;
            OnCollectibleUpdate();
        }

        public void Consume()
        {
            collectibleData.OnClaimReward();
            UpdateCollectiblesTarget();
            OnCollectibleUpdate();
        }

        public void OnResetGame()
        {
            collectibleData.ResetDataToStartValue();
            OnCollectibleUpdate();
        }

        public void Collect(List<int> collectibles)
        {
            AddCollectibles(collectibles.Count);
        }

        public void SpendCollectibles(int amount)
        {
            collectibleData.Spend(amount);
            OnCollectibleUpdate();
        }

        public bool IsEnoughCollectibleForMakingPurchase(int collectibleRequired)
        {
            return collectibleData.IsEnoughCollectiblesForMakingPurchase(collectibleRequired);
        }

        private void AddCollectibles(int amount)
        {
            collectibleData.AddCollectibles(amount);
            OnCollectibleUpdate();
        }

        private void OnCollectibleUpdate()
        {
            collectibleProgress.value = collectibleData.CurrentProgress();

            totalText.text = "Total: " + collectibleData.CollectibleTotal;

            consumeCollectiblesButton.interactable = collectibleData.DidTargetReach();

            if (collectibleData.DidTargetReach())
            {
                onTargetReached();
            }
        }


        private void UpdateCollectiblesTarget()
        {
            collectibleData.UpdateCollectibleTarget();
        }
    }
}