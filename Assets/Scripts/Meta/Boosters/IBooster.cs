﻿using System;
using System.Collections;
using System.Collections.Generic;
using BPLogic;
using UnityEngine;

namespace Meta.Boosters
{
    public interface IBooster
    {
        event Action onBoosterActivate;
        event Action onBoosterComplete;
        event Action onBoosterCancel;

        void Init(BPGame game);

        void Activate();
        void Complete(Vector2Int atIndex);
        void Cancel();
    }
}