﻿using System.Collections;
using System.Collections.Generic;
using BPCore;
using BPData.ProfileData;
using BPLogic;
using BPUI.PopUps;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Meta.Boosters
{
    public class BoosterBomb : BaseBooster
    {
        [SerializeField] private TextMeshProUGUI textBombCount;
        [SerializeField] private Image imageMoreBomb;

        private Vector3 defaultPosition;

        private bool isBombReady;
        
        public override void Init()
        {
            this.boosterType = EBoosterType.BoosterBomb;
            
            defaultPosition = transform.position;
            mainCamera = Camera.main;
        }

        public override void OnBeginDrag(PointerEventData eventData)
        {
            if (!isBombReady)
            {
                return;
            }

            SetMoving(mainCamera.ScreenToWorldPoint(eventData.position));
        }

        public override void OnDrag(PointerEventData eventData)
        {
            if (!isBombReady)
            {
                return;
            }

            SetMoving(mainCamera.ScreenToWorldPoint(eventData.position));
        }

        public override void OnEndDrag(PointerEventData eventData)
        {
            if (!isBombReady)
            {
                return;
            }

            HandleBoosterStopMoving(transform.position, Complete, Cancel);
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            if (!isBombReady)
            {
                OnNotEnoughBoosters();
            }
        }

        public override void OnBoostersDataLoaded(BoosterData data)
        {
            CheckBoomCount(data);
        }

        public override void OnBoostersDataUpdated(BoosterData data)
        {
            CheckBoomCount(data);
        }

        public override void Complete(Vector2Int atIndex)
        {
            transform.position = defaultPosition;
            bpGame.ApplyBoosterBoom(atIndex);
            base.Complete(atIndex);
            OnConsumingBoosters();
        }

        public override void Cancel()
        {
            transform.position = defaultPosition;
            base.Cancel();
        }

        private void CheckBoomCount(BoosterData data)
        {
            if (data.bombCount > 0)
            {
                HandleShowBombsCount(data.bombCount);
            }
            else
            {
                HandleOutOfBombs();
            }
        }

        private void HandleOutOfBombs()
        {
            isBombReady = false;
            textBombCount.enabled = false;
            imageMoreBomb.enabled = true;
        }

        private void HandleShowBombsCount(int bombCount)
        {
            isBombReady = true;
            textBombCount.enabled = true;
            textBombCount.text = bombCount.ToString();
            imageMoreBomb.enabled = false;
        }

        private void SetMoving(Vector3 worldPointer)
        {
            worldPointer.y += GameViewConfigs.MOVING_OFFSET;
            worldPointer.z = -10;

            transform.position = worldPointer;
        }
    }
}