﻿using System;
using System.Collections;
using System.Collections.Generic;
using Base;
using BPData.ProfileData;
using BPLogic;
using UnityEngine;

namespace Meta.Boosters
{
    public abstract class BaseBooster : BaseDraggable, IBooster
    {
        public event Action<Vector3, Action<Vector2Int>, Action> onBoosterStopMoving =
            delegate(Vector3 vector3, Action<Vector2Int> action, Action arg3) { };

        public event Action onBoosterActivate = delegate { };
        public event Action onBoosterComplete = delegate { };
        public event Action onBoosterCancel = delegate { };

        public event Action<EBoosterType> onConsumeBooster = delegate(EBoosterType type) {  };
        public event Action<EBoosterType> onNotEnoughBoosters = delegate(EBoosterType type) {  }; 

        protected BPGame bpGame;
        
        public abstract void OnBoostersDataLoaded(BoosterData data);

        public abstract void OnBoostersDataUpdated(BoosterData data);

        protected EBoosterType boosterType;


        protected void HandleBoosterStopMoving(Vector3 position, Action<Vector2Int> complete, Action cancel)
        {
            onBoosterStopMoving(position, complete, cancel);
        }

        public void Init(BPGame game)
        {
            bpGame = game;
        }

        public virtual void Activate()
        {
            onBoosterActivate();
        }

        public virtual void Complete(Vector2Int atIndex)
        {
            onBoosterComplete();
        }

        public virtual void Cancel()
        {
            onBoosterCancel();
        }

        protected void OnNotEnoughBoosters()
        {
            onNotEnoughBoosters(boosterType);
        }

        protected void OnConsumingBoosters()
        {
            onConsumeBooster(boosterType);
        }

    }
    
    public enum EBoosterType
    {
        BoosterBomb
    }
}