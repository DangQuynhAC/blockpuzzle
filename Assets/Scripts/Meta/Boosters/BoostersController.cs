﻿using System;
using System.Collections;
using System.Collections.Generic;
using BPCore;
using BPData.ProfileData;
using BPMeta;
using BPUI;
using BPUI.PopUps;
using BPViews;
using UnityEngine;

namespace Meta.Boosters
{
    public class BoostersController : ASystemController, IBoosterBombCallbackHandler
    {
        private List<BaseBooster> boosters;

        private BoardView boardView;

        private BoosterData data;

        public BoosterData Data => data;

        //TODO: find another way to inject dependencies
        private PopUpController popUpController;
        private CollectibleController collectibleController;

        public override void Init()
        {
            boosters = new List<BaseBooster>(FindObjectsOfType<BaseBooster>());
            for (int i = 0; i < boosters.Count; i++)
            {
                boosters[i].Init();
                boosters[i].onBoosterStopMoving += CheckBoosterDrop;
                boosters[i].onConsumeBooster += HandleConsumingBooster;
                boosters[i].onNotEnoughBoosters += HandleNotEnoughBoosters;
            }

            boardView = FindObjectOfType<BoardView>();

            collectibleController = FindObjectOfType<CollectibleController>();
            popUpController = FindObjectOfType<PopUpController>();
        }

        public override void OnProfileDataLoaded(ProfileData data)
        {
            for (int i = 0; i < boosters.Count; i++)
            {
                boosters[i].Init(data.game);
            }

            for (int i = 0; i < boosters.Count; i++)
            {
                boosters[i].OnBoostersDataLoaded(data.boosterData);
            }
        }

        private void CheckBoosterDrop(Vector3 position, Action<Vector2Int> complete, Action cancel)
        {
            int boardIndexCol = boardView.WorldToBoardIndexCol(position);
            int boardIndexRow = boardView.WorldToBoardIndexRow(position);

            if (!boardView.IsValidIndex(boardIndexCol, boardIndexRow))
            {
                cancel();
                return;
            }

            complete(new Vector2Int(boardIndexCol, boardIndexRow));
        }

        private void HandleNotEnoughBoosters(EBoosterType boosterType)
        {
            switch (boosterType)
            {
                case EBoosterType.BoosterBomb:
                    popUpController.Show(EPopUpID.BombShop, this, collectibleController);
                    break;
                default:
                    break;
            }
        }

        private void HandleConsumingBooster(EBoosterType type)
        {
            switch (type)
            {
                case EBoosterType.BoosterBomb:
                    data.ConsumeBomb();
                    break;

                default:
                    break;
            }

            HandleBoosterDataUpdated();
        }

        private void HandleAddingBoosters(EBoosterType type, int amount)
        {
            switch (type)
            {
                case EBoosterType.BoosterBomb:
                    data.AddBombs(amount);
                    break;

                default:
                    break;
            }

            HandleBoosterDataUpdated();
        }

        private void HandleBoosterDataUpdated()
        {
            for (int i = 0; i < boosters.Count; i++)
            {
                boosters[i].OnBoostersDataUpdated(data);
            }
        }

        public void AddBombs(int amount)
        {
            HandleAddingBoosters(EBoosterType.BoosterBomb, amount);
        }
    }
}