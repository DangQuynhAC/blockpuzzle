﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEditor;
// using UnityEngine;
//
// namespace Editor
// {
// #if UNITY_EDITOR
//     public class MPCAutoRun 
//     {
//         [InitializeOnEnterPlayMode]
//         public static void GenerateMPResolver()
//         {
//             System.Diagnostics.Process process = new System.Diagnostics.Process();
//             System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
//             startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
//             startInfo.FileName = "/bin/bash";
//             startInfo.Arguments = "dotnet mpc -i \"Assembly-CSharp.csproj\" -o \"../Assets/Generated/MessagePackGenerated.cs\" ";
//             process.StartInfo = startInfo;
//             process.StartInfo.UseShellExecute = true;
//             // process.StartInfo.RedirectStandardOutput = true;
//             process.Start();
//         }
//     }
//
// #endif
// }