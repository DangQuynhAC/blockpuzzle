﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

public class PrebuildProcess : IPreprocessBuildWithReport
{
    public int callbackOrder
    {
        get { return 0; }
    }

    public void OnPreprocessBuild(BuildReport report)
    {
        System.Diagnostics.Process process = new System.Diagnostics.Process();
        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
        startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
        startInfo.FileName = "/bin/bash";
        startInfo.Arguments =
            "dotnet mpc -i \"Assembly-CSharp.csproj\" -o \"../Assets/Generated/MessagePackGenerated.cs\" ";
        process.StartInfo = startInfo;
        process.StartInfo.UseShellExecute = true;
        // process.StartInfo.RedirectStandardOutput = true;
        process.Start();
    }
}